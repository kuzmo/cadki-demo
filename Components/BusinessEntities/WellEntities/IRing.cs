﻿namespace CadkiDemo.Components.BusinessEntities.WellEntities
{
    interface IRing
    {
        double Thickness { get; set; }
        double Diametr { get; set; }
        double InnerDiametr { get; }
    }
}
