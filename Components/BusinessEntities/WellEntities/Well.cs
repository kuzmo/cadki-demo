﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CadkiDemo.Components.BusinessEntities.WellEntities
{
    public class Well
    {
        public Well(List<AbstractWellComponent> components)
        {
            Components = components;
        }

        public string Name { get; set; }

        public List<AbstractWellComponent> Components { get; }

        public Position Position { get; private set; } = new Position(0, 0, 0);

        public double Height
        {
            get
            {
                return Components.Sum(x => x.Height);
            }
        }

        public List<Axis> GetCentrAxis()
        {
            var componentAxis = Components.Select(x => x.GetCenterAxis()).ToList();

            var resultAxis = new List<Axis>() { new Axis() { Start = componentAxis[0].Start, End = componentAxis[0].End } };
            for (int i = 1; i < componentAxis.Count; i++)
            {
                var lastAxis = resultAxis.Last();
                if (Math.Abs(lastAxis.Start.X - componentAxis[i].Start.X) < 0.001)
                {
                    lastAxis.End = componentAxis[i].End;
                    continue;
                }
                else
                {
                    resultAxis.Add(new Axis() { Start = componentAxis[i].Start, End = componentAxis[i].End });
                }
            }

            return resultAxis;
        }
    }
}
