﻿using System.Collections.Generic;

namespace CadkiDemo.Components.BusinessEntities.WellEntities
{
    /// <summary>
    /// Рабочая камера
    /// </summary>
    public class WorkingChamber : AbstractWellComponent
    {
        public double Diametr { get; set; }
        public double Thickness { get; set; }
        public double BottomThickness { get; set; }

        public override AbstractWellComponent Copy()
        {
            return (WorkingChamber)MemberwiseClone();
        }

        public override List<Axis> GetPlanAxes()
        {
            var xAxis = new Axis
            {
                Start = new Position(Position.X - Diametr / 2, Position.Y, Position.Z),
                End = new Position(Position.X + Diametr / 2, Position.Y, Position.Z)
            };

            var yAxis = new Axis
            {
                Start = new Position(Position.X, Position.Y - Diametr / 2, Position.Z),
                End = new Position(Position.X, Position.Y + Diametr / 2, Position.Z)
            };

            return new List<Axis> { xAxis, yAxis };
        }

        public override List<Position> GetDimSectionBot()
        {
            return new List<Position>();
        }

        public override Bound GetBound()
        {
            return new Bound
            {
                Start = new Position(Position.X - Diametr / 2, Position.Y - Diametr / 2, Position.Z),
                End = new Position(Position.X + Diametr / 2, Position.Y + Diametr / 2, Position.Z + Height)
            };
        }

        public override Isolation GetIsolation()
        {
            var leftPosiX = Position.X - Diametr / 2;
            var leftPosiY = Position.Y - Diametr / 2;
            var rightPosiX = Position.X + Diametr / 2;
            var rightPosiY = Position.Y + Diametr / 2;
            var isolationPoins = new List<Position>()
            {
                new Position(leftPosiX, leftPosiY, Position.Z),
                new Position(leftPosiX, leftPosiY, Position.Z + Height),
                new Position(leftPosiX + Thickness, leftPosiY + Thickness, Position.Z + Height),
                new Position(leftPosiX + Thickness, leftPosiY + Thickness, Position.Z + BottomThickness),
                new Position(rightPosiX  - Thickness, rightPosiY - Thickness, Position.Z + BottomThickness),
                new Position(rightPosiX  - Thickness, rightPosiY - Thickness, Position.Z + Height),
                new Position(rightPosiX, rightPosiY, Position.Z + Height),
                new Position(rightPosiX, rightPosiY , Position.Z)
            };

            return new Isolation(new List<List<Position>>() { isolationPoins });
        }
    }
}
