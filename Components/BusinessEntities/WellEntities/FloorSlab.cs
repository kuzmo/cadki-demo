﻿using System.Collections.Generic;

namespace CadkiDemo.Components.BusinessEntities.WellEntities
{
    /// <summary>
    /// Плита перекрытия
    /// </summary>
    public class FloorSlab : AbstractWellComponent
    {
        public double Diametr { get; set; }
        public double HoleDiametr { get; set; }
        public double HoleOffset { get; set; }
        public double Thickness { get; set; }
        public override double Height => Thickness;

        public override AbstractWellComponent Copy()
        {
            return (FloorSlab)MemberwiseClone();
        }

        public override List<Axis> GetPlanAxes()
        {
            var xAxis = new Axis
            {
                Start = new Position(Position.X - Diametr / 2, Position.Y, Position.Z),
                End = new Position(Position.X + Diametr / 2, Position.Y, Position.Z)
            };

            var yAxis = new Axis
            {
                Start = new Position(Position.X, Position.Y - Diametr / 2, Position.Z),
                End = new Position(Position.X, Position.Y + Diametr / 2, Position.Z)
            };

            return new List<Axis> { xAxis, yAxis };
        }

        public override List<Position> GetDimSectionBot()
        {
            return new List<Position>
            {
                new Position(Position.X - Diametr / 2, Position.Y, Position.Z),
                new Position(Position.X + Diametr / 2, Position.Y, Position.Z),
            };
        }

        public override Bound GetBound()
        {
            return new Bound
            {
                Start = new Position(Position.X - Diametr / 2, Position.Y - Diametr / 2, Position.Z),
                End = new Position(Position.X + Diametr / 2, Position.Y + Diametr / 2, Position.Z + Height)
            };
        }

        public override Isolation GetIsolation()
        {
            var redius = Diametr / 2;
            var holeStart = HoleOffset - HoleDiametr / 2;
            var holeEnd = HoleOffset + HoleDiametr / 2;

            var leftIsolationPoints = new List<Position>
            {
                new Position(-redius + Position.X, -redius + Position.Y, Position.Z),
                new Position(-redius + Position.X, -redius + Position.Y, Position.Z + Height),
                new Position(holeStart + Position.X, Position.Y, Position.Z + Height),
                new Position(holeStart + Position.X, Position.Y, Position.Z)
            };

            var rightIsolationPoints = new List<Position>
            {
                new Position(holeEnd + Position.X, Position.Y, Position.Z),
                new Position(holeEnd + Position.X, Position.Y, Position.Z + Height),
                new Position(redius + Position.X, redius + Position.Y, Position.Z + Height),
                new Position(redius + Position.X, redius + Position.Y, Position.Z)
            };

            return new Isolation(new List<List<Position>>() { leftIsolationPoints, rightIsolationPoints });
        }
    }
}
