﻿using CadkiDemo.Components.BusinessEntities.Abstract;
using System.Collections.Generic;

namespace CadkiDemo.Components.BusinessEntities.WellEntities
{
    public abstract class AbstractWellComponent : AbstractDrawComponent
    {
        public string GOST { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public virtual double Height { get; set; }
        public bool IsEven { get; private set; }

        public void SetOrder(int i)
        {
            IsEven = i % 2 == 0;
        }

        public virtual Axis GetCenterAxis()
        {
            var start = Position;
            var end = new Position(Position.X, Position.Y, Position.Z + Height);
            return new Axis { Start = start, End = end };
        }

        public abstract List<Axis> GetPlanAxes();

        public abstract Isolation GetIsolation();
        public abstract AbstractWellComponent Copy();
        public abstract Bound GetBound();

        /// <summary>
        /// Характерные точки для образмеривания - сечения, снизу
        /// </summary>
        public abstract List<Position> GetDimSectionBot();
    }
}
