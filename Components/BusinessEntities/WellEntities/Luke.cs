﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadkiDemo.Components.BusinessEntities.WellEntities
{
    public class Luke : AbstractWellComponent
    {
        public double Diametr { get; set; }

        public Luke() { }

        public override AbstractWellComponent Copy()
        {
            return (Luke)MemberwiseClone();
        }

        public override Bound GetBound()
        {
            return new Bound
            {
                Start = new Position(Position.X - Diametr / 2, Position.Y - Diametr / 2, Position.Z),
                End = new Position(Position.X + Diametr / 2, Position.Y + Diametr / 2, Position.Z + Height)
            };
        }

        public override List<Position> GetDimSectionBot()
        {
            throw new NotImplementedException();
        }

        public override Isolation GetIsolation()
        {
            return new Isolation(new List<List<Position>> { });
        }

        public override List<Axis> GetPlanAxes()
        {
            var xAxis = new Axis
            {
                Start = new Position(Position.X - Diametr / 2, Position.Y, Position.Z),
                End = new Position(Position.X + Diametr / 2, Position.Y, Position.Z)
            };

            var yAxis = new Axis
            {
                Start = new Position(Position.X, Position.Y - Diametr / 2, Position.Z),
                End = new Position(Position.X, Position.Y + Diametr / 2, Position.Z)
            };

            return new List<Axis> { xAxis, yAxis };
        }
    }
}
