﻿using System.Collections.Generic;

namespace CadkiDemo.Components.BusinessEntities.WellEntities
{
    /// <summary>
    /// Опорная плита
    /// </summary>
    public class BasePlate : AbstractWellComponent
    {
        public double Diametr { get; set; }
        public double Length { get; set; }
        public double Width { get; set; }
        public double Thickness { get; set; }
        public override double Height => Thickness;

        public override AbstractWellComponent Copy()
        {
            return (BasePlate)MemberwiseClone();
        }

        public override List<Axis> GetPlanAxes()
        {
            var xAxis = new Axis
            {
                Start = new Position(Position.X - Length / 2, Position.Y, Position.Z),
                End = new Position(Position.X + Length / 2, Position.Y, Position.Z)
            };

            var yAxis = new Axis
            {
                Start = new Position(Position.X, Position.Y - Height / 2, Position.Z),
                End = new Position(Position.X, Position.Y + Height / 2, Position.Z)
            };

            return new List<Axis> { xAxis, yAxis };
        }

        public override List<Position> GetDimSectionBot()
        {
            return new List<Position>
            {
                new Position(Position.X - Length / 2, Position.Y, Position.Z),
                new Position(Position.X + Length / 2, Position.Y, Position.Z),
            };
        }

        public override Bound GetBound()
        {
            return new Bound
            {
                Start = new Position(Position.X - Length / 2, Position.Y - Width / 2, Position.Z),
                End = new Position(Position.X + Length / 2, Position.Y + Width / 2, Position.Z + Height)
            };
        }

        public override Isolation GetIsolation()
        {
            var leftPosiX = Position.X - Length / 2;
            var leftPosiY = Position.Y - Height / 2;
            var rightPosiX = Position.X + Length / 2;
            var rightPosiY = Position.Y + Height / 2;
            var halfDiametr = Diametr / 2;
            var leftIsolationPoins = new List<Position>()
            {
                new Position(leftPosiX, leftPosiY, Position.Z),
                new Position(leftPosiX, leftPosiY, Position.Z + Height),
                new Position(-halfDiametr, -halfDiametr, Position.Z + Height),
                new Position(-halfDiametr, -halfDiametr, Position.Z)
            };

            var rightIsolationPoins = new List<Position>()
            {
                new Position(halfDiametr, halfDiametr, Position.Z),
                new Position(halfDiametr, halfDiametr, Position.Z + Height),
                new Position(rightPosiX, rightPosiY, Position.Z + Height),
                new Position(rightPosiX, rightPosiY, Position.Z)
            };

            return new Isolation(new List<List<Position>>() { leftIsolationPoins, rightIsolationPoins });
        }
    }
}
