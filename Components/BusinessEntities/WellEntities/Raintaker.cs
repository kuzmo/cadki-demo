﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadkiDemo.Components.BusinessEntities.WellEntities
{
    public class Raintaker : AbstractWellComponent
    {
        public double Length { get; set; }
        public double Width { get; set; }
        public Raintaker() { }

        public override AbstractWellComponent Copy()
        {
            return (Raintaker)MemberwiseClone();
        }

        public override Bound GetBound()
        {
            return new Bound
            {
                Start = new Position(Position.X - Length / 2, Position.Y - Width / 2, Position.Z),
                End = new Position(Position.X + Length / 2, Position.Y + Width / 2, Position.Z + Height)
            };
        }

        public override List<Position> GetDimSectionBot()
        {
            throw new NotImplementedException();
        }

        public override Isolation GetIsolation()
        {
            return new Isolation(new List<List<Position>> { });
        }

        public override List<Axis> GetPlanAxes()
        {
            var xAxis = new Axis
            {
                Start = new Position(Position.X - Length / 2, Position.Y, Position.Z),
                End = new Position(Position.X + Length / 2, Position.Y, Position.Z)
            };

            var yAxis = new Axis
            {
                Start = new Position(Position.X, Position.Y - Width / 2, Position.Z),
                End = new Position(Position.X, Position.Y + Width / 2, Position.Z)
            };

            return new List<Axis> { xAxis, yAxis };
        }
    }
}
