﻿using System.Collections.Generic;

namespace CadkiDemo.Components.BusinessEntities.WellEntities
{
    /// <summary>
    /// Стеновое кольцо
    /// </summary>
    public class Ring : AbstractWellComponent, IRing
    {
        public double Thickness { get; set; }
        public double Diametr { get; set; }
        public double InnerDiametr
        {
            get
            {
                return Diametr - (Thickness * 2);
            }
        }
        public Ring() { }

        public override AbstractWellComponent Copy()
        {
           return (Ring)MemberwiseClone();
        }

        public override List<Axis> GetPlanAxes()
        {
            var xAxis = new Axis
            {
                Start = new Position(Position.X - Diametr / 2, Position.Y, Position.Z),
                End = new Position(Position.X + Diametr / 2, Position.Y, Position.Z)
            };

            var yAxis = new Axis
            {
                Start = new Position(Position.X, Position.Y - Diametr / 2, Position.Z),
                End = new Position(Position.X, Position.Y + Diametr / 2, Position.Z)
            };

            return new List<Axis> { xAxis, yAxis };
        }

        public override List<Position> GetDimSectionBot()
        {
            return new List<Position>
            {
                new Position(Position.X - Diametr / 2, Position.Y, Position.Z),
                new Position(Position.X + Diametr / 2, Position.Y, Position.Z)
            };
        }

        public override Bound GetBound()
        {
            return new Bound
            {
                Start = new Position(Position.X - Diametr / 2, Position.Y - Diametr / 2, Position.Z),
                End = new Position(Position.X + Diametr / 2, Position.Y + Diametr / 2, Position.Z + Height)
            };
        }

        public override Isolation GetIsolation()
        {
            var leftPosiX = Position.X - Diametr / 2;
            var leftPosiY = Position.Y - Diametr / 2;
            var rightPosiX = Position.X + Diametr / 2;
            var rightPosiY = Position.Y + Diametr / 2;
            var leftIsolationPoins = new List<Position>()
            {
                new Position(leftPosiX, leftPosiY, Position.Z),
                new Position(leftPosiX, leftPosiY, Position.Z + Height),
                new Position(leftPosiX + Thickness, leftPosiY + Thickness, Position.Z + Height),
                new Position(leftPosiX + Thickness, leftPosiY + Thickness, Position.Z)
            };

            var rightIsolationPoins = new List<Position>()
            {
                new Position(rightPosiX  - Thickness, rightPosiY - Thickness, Position.Z),
                new Position(rightPosiX  - Thickness, rightPosiY  - Thickness, Position.Z + Height),
                new Position(rightPosiX, rightPosiY, Position.Z + Height),
                new Position(rightPosiX, rightPosiY , Position.Z)
            };

            return new Isolation(new List<List<Position>>() { leftIsolationPoins, rightIsolationPoins });
        }
    }
}
