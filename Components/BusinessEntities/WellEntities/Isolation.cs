﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadkiDemo.Components.BusinessEntities.WellEntities
{
    public class Isolation
    {
        public List<List<Position>> Points { get; private set; }

        public Isolation(List<List<Position>> points)
        {
            Points = points;
        }
    }
}
