namespace CadkiDemo.Components.BusinessEntities.Abstract
{
    public abstract class AbstractDrawComponent
    {
        public string Name { get; set; }
        public Position Position { get; set; }

        public virtual void SetPosition(double x, double y, double z)
        {
            Position = new Position(x, y, z);
        }
    }
}