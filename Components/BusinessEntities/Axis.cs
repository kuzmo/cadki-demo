﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadkiDemo.Components.BusinessEntities
{
    public class Axis
    {
        public Position Start { get; set; }
        public Position End { get; set; }
    }
}
