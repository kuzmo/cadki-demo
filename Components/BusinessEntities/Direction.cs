﻿namespace CadkiDemo.Components.BusinessEntities
{
    public struct Direction
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public Direction(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Direction(double[] array)
        {
            if (array.Length == 1)
            {
                X = array[0];
                Y = 0;
                Z = 0;
            }
            else if (array.Length == 2)
            {
                X = array[0];
                Y = array[1];
                Z = 0;
            }
            else
            {
                X = array[0];
                Y = array[1];
                Z = array[2];
            }
        }
    }
}
