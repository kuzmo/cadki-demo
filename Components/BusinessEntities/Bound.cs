﻿namespace CadkiDemo.Components.BusinessEntities
{
    public struct Bound
    {
        public Position Start { get; set; }
        public Position End { get; set; }

        public Position GetMiddle()
        {
            return (Start + End) * 0.5;
        }
    }
}
