﻿using System;

namespace CadkiDemo.Components
{
    public struct Position
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public Position(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Position(double[] coords)
        {
            if (coords.Length == 1)
            {
                X = coords[0];
                Y = 0;
                Z = 0;
            }
            else if (coords.Length == 2)
            {
                X = coords[0];
                Y = coords[1];
                Z = 0;
            }
            else
            {
                X = coords[0];
                Y = coords[1];
                Z = coords[2];
            }
        }

        public static Position operator +(Position p1, Position p2)
        {
            return new Position(p1.X + p2.X, p1.Y + p2.Y, p1.Z + p2.Z);
        }

        public static Position operator -(Position p1, Position p2)
        {
            return new Position(p1.X - p2.X, p1.Y - p2.Y, p1.Z - p2.Z);
        }

        public static Position operator *(Position p1, double k)
        {
            return new Position(p1.X * k, p1.Y * k, p1.Z * k);
        }

        public bool Equals(object obj, double tolerance)
        {
            if (obj == null) return false;
            if (this.GetType() != obj.GetType()) return false;
            var point = (Position)obj;

            return Math.Abs(this.X - point.X) < tolerance &&
                Math.Abs(this.Y - point.Y) < tolerance &&
                Math.Abs(this.Z - point.Z) < tolerance;
        }

    }
}
