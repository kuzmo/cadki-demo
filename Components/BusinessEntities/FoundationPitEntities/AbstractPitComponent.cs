﻿using CadkiDemo.Components.BusinessEntities.Abstract;

namespace CadkiDemo.Components.BusinessEntities.FoundationPitEntities
{
    public abstract class AbstractPitComponent : AbstractDrawComponent
    {
        public double TopWidth { get; set; }
        public double BottomWidth { get; set; }
        public double TopMark { get; set; }
        public double BottomMark { get; set; }
        public double LeftSkew { get; set; } = 0.785398;
        public double RightSkew { get; set; } = 0.785398;

        public double Height
        {
            get
            {
                return (TopMark - BottomMark) * 1000;
            }
        }
    }
}
