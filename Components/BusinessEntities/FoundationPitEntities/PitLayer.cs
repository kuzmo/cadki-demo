﻿namespace CadkiDemo.Components.BusinessEntities.FoundationPitEntities
{
    public enum LayerType
    {
        Sand,
        Soil,
        CrushedStone
    }

    public class PitLayer : AbstractPitComponent
    {
        public LayerType LayerType { get; set; }

        public PitLayer(double bottomMark, double topMark, LayerType type)
        {
            TopMark = topMark;
            BottomMark = bottomMark;
            LayerType = type;
        }
    }
}
