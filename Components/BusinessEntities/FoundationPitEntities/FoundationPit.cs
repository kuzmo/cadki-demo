﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CadkiDemo.Components.BusinessEntities.FoundationPitEntities
{
    public class FoundationPit : AbstractPitComponent
    {
        public List<PitLayer> PitLayers = new List<PitLayer>();

        public FoundationPit(double bottomMark, double topMark, double bottomWith, double topWith)
        {
            BottomMark = bottomMark;
            TopMark = topMark;
            BottomWidth = bottomWith;
            TopWidth = topWith;
        }

        public void AddPitLayers(List<PitLayer> layers)
        {
            var orderedLayers = layers.OrderBy(x => x.BottomMark).ToList();
            var bottomWidth = BottomWidth;
            var height = 0.0;
            var dx = 0.0;
            var dy = 0.0;
            for (int i = 0; i < orderedLayers.Count; i ++)
            {
                var layer = orderedLayers[i];
                layer.LeftSkew = LeftSkew;
                layer.RightSkew = RightSkew;
                layer.BottomWidth = bottomWidth;
                layer.TopWidth = layer.BottomWidth + layer.Height / Math.Tan(LeftSkew) + layer.Height / Math.Tan(RightSkew);
                layer.Position = new Position(dx, dx, height);
                PitLayers.Add(layer);
                bottomWidth = layer.TopWidth;
                height += layer.Height;
                dx -= layer.Height / Math.Tan(LeftSkew);
                dy -= layer.Height / Math.Tan(LeftSkew);
            }
        }
    }
}
