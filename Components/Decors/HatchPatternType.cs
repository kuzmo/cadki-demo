namespace CadkiDemo.Components.Decors
{
    public enum PatternType
    {
        UserDefined,
        PreDefined,
        CustomDefined,
    }
}