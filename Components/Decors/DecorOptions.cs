using System.Drawing;

namespace CadkiDemo.Components.Decors
{
    public class DecorOptions
    {
        public double Scale { get; set; } = 50;

        /// <summary>
        /// Сплошная толстая
        /// </summary>
        public EntityDecor DecorContinuousThick { get; set; } = new EntityDecor
        {
            Layer = new LayerInfo
            {
                Name = "Основные",
                LineWeight = 30,
            }
        };

        /// <summary>
        /// Сплошная тонкая
        /// </summary>
        public EntityDecor DecorContinuousThin { get; set; } = new EntityDecor
        {
            Layer = new LayerInfo
            {
                Name = "Тонкие",
                LineWeight = 15,
            }
        };

        /// <summary>
        /// Штриховая
        /// </summary>
        public EntityDecor DecorDot { get; set; } = new EntityDecor
        {
            Layer = new LayerInfo
            {
                Name = "Штриховые",
                LineWeight = 15,
                LineType = "Штриховая"
            }
        };

        /// <summary>
        /// Осевая
        /// </summary>
        public EntityDecor DecorAxis { get; set; } = new EntityDecor
        {
            Layer = new LayerInfo
            {
                Name = "Осевые",
                LineWeight = 15,
                LineType = "Осевая",
                Color = Color.Red
            }
        };

        /// <summary>
        /// Изоляция
        /// </summary>
        public EntityDecor DecorIsolation { get; set; } = new EntityDecor
        {
            Layer = new LayerInfo
            {
                Name = "Изоляция",
                LineWeight = 30,
                Color = Color.Magenta
            }
        };

        public EntityDecor DecorDim { get; set; } = new EntityDecor
        {
            Layer = new LayerInfo
            {
                Name = "Размеры",
                LineWeight = 15,
                Color = Color.Violet
            }
        };

        public EntityDecor DecorLeader { get; set; } = new EntityDecor
        {
            Layer = new LayerInfo
            {
                Name = "Выноски",
                LineWeight = 15,
                Color = Color.Violet
            }
        };

        public EntityDecor DecorText { get; set; } = new EntityDecor
        {
            Layer = new LayerInfo
            {
                Name = "Текст",
                LineWeight = 15,
                Color = Color.Violet
            }
        };

        /// <summary>
        /// Штриховка бетон
        /// </summary>
        public HatchDecor DecorConcrete { get; set; } = new HatchDecor
        {
            PatternType = PatternType.UserDefined,
            PatternName = "ANSI31",
            Layer = new LayerInfo
            {
                Name = "Штриховка",
                LineWeight = 15
            }
        };

        /// <summary>
        /// Штриховка земля
        /// </summary>
        public HatchDecor DecorEarth { get; set; } = new HatchDecor
        {
            PatternType = PatternType.UserDefined,
            PatternName = "AR-HBONE",
            PatternScaleFactor = 0.015,
            Layer = new LayerInfo
            {
                Name = "Штриховка",
                LineWeight = 15
            }
        };

        public string TableStyleName { get; set; } = "Cadki";
        public string TextStyleName { get; set; } = "Cadki";
        public string DimStyleName { get; set; } = "Cadki";
        public string LeaderStyleName { get; set; } = "Cadki";
    }
}