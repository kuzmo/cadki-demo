using System.Drawing;

namespace CadkiDemo.Components.Decors
{
    public class EntityDecor
    {
        public LayerInfo Layer { get; set; }
        public int? LineWeight { get; set; }
        public string LineType { get; set; }
        public Color Color { get; set; }
    }

    public class LayerInfo
    {
        public string Name { get; set; }
        public int? LineWeight { get; set; }
        public string LineType { get; set; }
        public Color Color { get; set; }
    }
}