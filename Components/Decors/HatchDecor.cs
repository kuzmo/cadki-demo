namespace CadkiDemo.Components.Decors
{
    public class HatchDecor : EntityDecor
    {
        public string PatternName { get; set; }
        public PatternType PatternType { get; set; }
        public double PatternScaleFactor = 1;
    }
}