﻿using CadkiDemo.Components.BusinessEntities.WellEntities;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;

namespace CadkiDemo.Components
{
    public class JsonConverter : CustomCreationConverter<AbstractWellComponent>
    {
        public override AbstractWellComponent Create(Type objectType)
        {
            throw new NotImplementedException();
        }

        public AbstractWellComponent Create(Type objectType, JObject jObject)
        {
            var type = (string)jObject.Property("type");

            switch (type)
            {
                case "ring":
                    return new Ring();
                case "working_chamber":
                    return new WorkingChamber();
                case "support_ring":
                    return new SupportRing();
                case "base_plate":
                    return new BasePlate();
                case "road_plate":
                    return new RoadPlate();
                case "bottom_plate":
                    return new BottomPlate();
                case "floor_slab":
                    return new FloorSlab();
                case "luke":
                    return new Luke();
                case "raintaker":
                    return new Raintaker();
            }

            throw new ApplicationException(String.Format("The construction type {0} is not supported!", type));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            // Load JObject from stream
            JObject jObject = JObject.Load(reader);

            // Create target object based on JObject
            var target = Create(objectType, jObject);

            // Populate the object properties
            serializer.Populate(jObject.CreateReader(), target);

            return target;
        }
    }
}
