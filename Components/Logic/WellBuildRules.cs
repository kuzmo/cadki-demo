﻿using CadkiDemo.Components.BusinessEntities.WellEntities;

namespace CadkiDemo.Components.Logic
{
    public class WellBuildRules
    {
        public void Build_1(Well well)
        {
            var acc = 0.0;
            for (int i = 0; i < well.Components.Count; i++)
            {
                well.Components[i].SetPosition(well.Position.X, well.Position.Y, well.Position.Z + acc);
                well.Components[i].SetOrder(i);
                acc += well.Components[i].Height;
            }
        }

        public void Build_2(Well well)
        {
            var position = well.Position;
            var zOffset = 0.0;
            var xOffset = 0.0;
            FloorSlab floorSlab = null;

            for (int i = 0; i < well.Components.Count; i++)
            {
                var component = well.Components[i];
                var ring = component as IRing;
                if (ring != null && floorSlab != null)
                {
                    xOffset = floorSlab.HoleOffset + floorSlab.HoleDiametr / 2 - ring.InnerDiametr / 2 + xOffset;
                }

                floorSlab = component as FloorSlab;
                component.SetPosition(position.X + xOffset, position.Y, position.Z + zOffset);
                component.SetOrder(i);
                zOffset += well.Components[i].Height;
            }
        }
    }
}
