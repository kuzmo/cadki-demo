using System;

namespace CadkiDemo.Components.Exceptions
{
    /// <summary>
    /// Ошибка в программе
    /// </summary>
    public class BusinessLogicException : Exception
    {
    }
}