using System.IO;

namespace CadkiDemo.Components.Repositories
{
    public class FileRepository
    {
        /// <summary>
        /// Файл из репозитория ресурсов
        /// </summary>
        /// <param name="relativePath">Путь от репозитория</param>
        /// <returns>Полный путь к файлу</returns>
        public string GetFile(string relativePath)
        {
            var dir = Path.GetDirectoryName(GetType().Assembly.Location);
            var file = Path.Combine(dir, $@"Resources\{relativePath}");
            return file;
        }
    }
}