using System.IO;

namespace CadkiDemo.Components.Repositories
{
    public class BlockRepository
    {
        public string LoadBlockFile(string gost, string componentName)
        {
            var dir = Path.GetDirectoryName(GetType().Assembly.Location);
            var file = Path.Combine(dir, $@"Resources\Blocks\{gost}\{componentName}.dwg");
            return !File.Exists(file) ? null : file;
        }
    }
}