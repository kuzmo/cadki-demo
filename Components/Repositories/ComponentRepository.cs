﻿using CadkiDemo.Components.BusinessEntities.WellEntities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CadkiDemo.Components.Repositories
{
    public class ComponentRepository
    {
        private List<AbstractWellComponent> _components;
        public ComponentRepository()
        {
            _components = ReadJsonFiles();
            foreach (var component in _components)
            {
                component.Type = GetTypeTranslate(component.Type);
            }
        }

        private List<AbstractWellComponent> ReadJsonFiles()
        {
            var result = new List<AbstractWellComponent>();
            var dir = Path.Combine(Path.GetDirectoryName(GetType().Assembly.Location), "Resources");
            var files = Directory.GetFiles(dir, "GOST_*.json");
            foreach (var file in files)
            {
                var json = File.ReadAllText(file);
                var components = JsonConvert.DeserializeObject<List<AbstractWellComponent>>(json, new JsonConverter());
                result.AddRange(components);
            }    
            return result;
        }

        public IEnumerable<AbstractWellComponent> GetComponents(string type, string gost)
        {
            return _components.Where(w => w.Type == type && w.GOST == gost);
        }

        public IEnumerable<string> GetTypes()
        {
            return _components.Select(s => s.Type).Distinct();
        }

        public IEnumerable<string> GetGosts(string type)
        {
            return _components.Where(w => w.Type == type).Select(s => s.GOST).Distinct();
        }

        private static string GetTypeTranslate(string type)
        {
            switch (type)
            {
                case "ring": return "Стеновое кольцо";
                case "working chamber": return "Рабочая камера";
                case "support ring": return "Опорное кольцо";
                case "base plate": return "Опорная плита";
                case "road plate": return "Дорожная плита";
                case "bottom plate": return "Плита днища";
                case "floor slab": return "Плита перекрытия";
                case "luke": return "Люк";
                case "raintaker": return "Дождеприемник";
                default: return type;
            }
        }
    }
}
