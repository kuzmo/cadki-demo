﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UI.Views;

namespace UI.ViewModels
{
    public class MainWindowVM : BaseVM
    {

        public MainWindowVM(OrderList ordersView)
        {
            OrdersContent = ordersView;
        }
        public object OrdersContent { get; set; }
    }
}
