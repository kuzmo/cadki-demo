﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace UI.ViewModels
{
    public class OrderVM : BaseVM
    {
        private Main _main;
        private string _status;
        private ICommand _draw;

        public string Id { get; set; }
        public DateTime Date { get; set; }

        public string InstanceId { get; set; }

        public OrderVM(Main main)
        {
            _main = main;
        }

        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                if (value != _status)
                {
                    _status = value;
                    OnPropertyChanged("Status");
                }
            }
        }

        public ICommand Draw
        {
            get
            {
                return _draw ?? (_draw = new CommandHandler((parameter) => {
                    _main.Draw(this);
                }, true));
            }
        }
    }
}
