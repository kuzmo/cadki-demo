﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI.Models
{
    class OrdersResponse
    {
        public int total { get; set; }
        public List<Order> result { get; set; }
    }
}
