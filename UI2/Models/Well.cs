﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI.Models
{

    public class WellData
    {
        public string[] components { get; set; }
    }

    public class Well
    {
        public string _id { get; set; }
        public WellData data { get; set; }
    }
}
