﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading.Tasks;

namespace UI.Models
{
    class OrderResult
    {
        public bool isNew { get; set; }

        public string objectId { get; set; }
    }

    class Order
    {
        public string _id { get; set; }
        public string status { get; set; }
        public OrderResult result { get; set; }
        public DateTime date { get; set; }
    }
}
