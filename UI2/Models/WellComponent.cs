﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI.Models
{
    class Schema
    {
        public string name;
    }
    class WellComponent
    {
        public Dictionary<string, object> instance;

        [JsonProperty("object")]
        public Schema schema;
    }
}
