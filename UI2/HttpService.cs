﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace UI
{
    public class HttpService
    {
        private readonly string _url;
        private readonly HttpClient _httpClien;
        public CookieContainer Cookies { get; set; }

        public HttpService()
        {
            Cookies = new CookieContainer();
            HttpClientHandler handler = new HttpClientHandler
            {
                CookieContainer = Cookies
            };

            _url = "https://backend.cadki.xyz";
            _httpClien = new HttpClient(handler);
        }

        public Task<HttpResponseMessage> SignIn()
        {
            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("username", "user1"),
                new KeyValuePair<string, string>("password", "a-1")
            });

            var req = new HttpRequestMessage(HttpMethod.Post, _url + "/api/users/signin")
            {
                Content = formContent
            };

            return _httpClien.SendAsync(req);
        }

        public Task<HttpResponseMessage> GetOrders()
        {
            return _httpClien.GetAsync($"{_url}/api/instances/orders?objectId=5f9e7e2a05d4f10013fe0999");
        }

        public Task<HttpResponseMessage> GetWell(string instanceId)
        {
            return _httpClien.GetAsync($"{_url}/api/instances/5f9e7e2a05d4f10013fe0999/{instanceId}");
        }

        public Task<HttpResponseMessage> GetWellComponent(string instanceId)
        {
            return _httpClien.GetAsync($"{_url}/api/objects/instance/{instanceId}");
        }
    }
}
