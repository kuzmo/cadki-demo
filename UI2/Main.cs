﻿using CadkiDemo.Components.BusinessEntities.WellEntities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UI.Models;
using UI.ViewModels;
using UI.Views;
using Well = UI.Models.Well;

namespace UI
{
    public class Main
    {
        private HttpService _service = new HttpService();
        private CadkiDemo.Components.JsonConverter WellJsonConverter = new CadkiDemo.Components.JsonConverter();

        public event DrawHandler OnDraw;
        public delegate void DrawHandler(OrderVM orderVM);

        public MainWindowVM MainWindowVM { get ; set; }

        public Main()
        {
            var ordersVM = new OrderListVM();
            ordersVM.Items = new ObservableCollection<OrderVM>();
            var ordersView = new OrderList(ordersVM);
            MainWindowVM = new MainWindowVM(ordersView);
            this.init(ordersVM);
        }

        private async void init(OrderListVM ordersVM)
        {
            await _service.SignIn();
            this.OrderUpdatingJob(ordersVM);
        }

        private void OrderUpdatingJob(OrderListVM ordersVM)
        {
            var uiContext = SynchronizationContext.Current;


            new Thread(async () =>
            {
                Thread.CurrentThread.IsBackground = true;
                while (true)
                {
                    Thread.Sleep(3000);
                    var rr = await _service.GetOrders();
                    var json = await rr.Content.ReadAsStringAsync();
                    var response = JsonConvert.DeserializeObject<OrdersResponse>(json);
                    uiContext.Send(y => this.UpdateList(response.result, ordersVM.Items), null);
                }
            }).Start();
        }

        private void UpdateList(List<Order> newOrders, ObservableCollection<OrderVM> oldOrders)
        {
            newOrders.ForEach(x =>
            {
                var existed = oldOrders.ToList().Find(y => y.Id.Equals(x._id));
                if (existed == null)
                {

                    var orderVm = new OrderVM(this) { Id = x._id, Status = x.status };
                    oldOrders.Add(orderVm);
                    if (x.result != null)
                    {
                        orderVm.InstanceId = x.result.objectId;
                    }
                }
                else
                {
                    existed.Status = x.status;
                    if (x.result != null)
                    {
                        existed.InstanceId = x.result.objectId;
                    }
                }
            });
        }

        public async Task<List<AbstractWellComponent>> GetWellData(string wellId)
        {

            var wellContent = await _service.GetWell(wellId);
            var wellJson = await wellContent.Content.ReadAsStringAsync();
            var well = JsonConvert.DeserializeObject<Well>(wellJson);
            var wellComponetns = new List<AbstractWellComponent>();
            foreach (var compId in well.data.components)
            {
                var x = await _service.GetWellComponent(compId);
                var xJson = await x.Content.ReadAsStringAsync();
                var comp = JsonConvert.DeserializeObject<WellComponent>(xJson);
                comp.instance.Add("type", comp.schema.name);
                var localResult = JsonConvert.DeserializeObject<AbstractWellComponent>(JsonConvert.SerializeObject(comp.instance), WellJsonConverter);
                wellComponetns.Add(localResult);
            }
            return wellComponetns;
        }

        public void Draw(OrderVM orderVM)
        {
            OnDraw(orderVM);
        }

    }
}
