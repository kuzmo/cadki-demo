using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Extension;

namespace CadkiDemo.Utils.Dims
{
    public class DimLineBottom
    {
        public List<Entity> GetDim(List<Point2d> dimPtsBot, double dimY, ObjectId dimStyleId)
        {
            var dims = new List<Entity>();
            List<(double X, Point2d Pt)> orderedX = dimPtsBot.Select(s => (s.X, s)).OrderBy(o => o.X).ToList();
            var start = orderedX.First();
            foreach (var end in orderedX.Skip(1).Where(w => Math.Abs(w.X - start.X) > 0.1))
            {
                var pt1 = start.Pt.Point3d();
                var pt2 = end.Pt.Point3d();
                var dimPt = new Point3d(start.X + (end.X - start.X) * 0.5, dimY, 0);
                var dim = new RotatedDimension(0, pt1, pt2, dimPt, null, dimStyleId);
                dims.Add(dim);
                start = end;
            }

            return dims;
        }
    }
}