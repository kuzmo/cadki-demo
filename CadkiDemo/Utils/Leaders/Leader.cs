﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace CadkiDemo.Utils.Leaders
{
    public class Leader
    {
        private ObjectId styleId;

        public Leader(ObjectId leaderStyleId)
        {
            styleId = leaderStyleId;
        }

        public Entity GetLeader(Point3d ptBase, Point3d ptText, string text)
        {
            var mtext = new MText
            {
                Contents = text
            };

            var leader = new MLeader
            {
                MText = mtext,
                TextLocation = ptText,
                MLeaderStyle = styleId
            };

            leader.AddLeaderLine(ptBase);
            return leader;
        }
    }
}
