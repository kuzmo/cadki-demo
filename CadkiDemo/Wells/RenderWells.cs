using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Components.Decors;
using CadkiDemo.Components.Logic;
using CadkiDemo.ComponentViews;
using CadkiDemo.Extension;
using CadkiDemo.Wells.Jig;
using Application = Autodesk.AutoCAD.ApplicationServices.Application;

namespace CadkiDemo.Wells
{
    public class RenderWells
    {
        private ViewFactory _viewFactory;
        private Document _doc;
        private Database _db;

        public void DrawWells(List<Well> wells, WellViewType viewType, DecorOptions decor)
        {
            _doc = Application.DocumentManager.MdiActiveDocument;
            _db = _doc.Database;
            _viewFactory = new ViewFactory();
            var rules = new WellBuildRules();
            var wellsEnts = new List<Entity>();
            var layoutPt = new Point3d();

            using var _ = _doc.LockDocument();
            using var t = _doc.TransactionManager.StartTransaction();
            using var cs = _db.CS(OpenMode.ForWrite);

            var decorAcad = new DecorOptionsAcad(decor)
            {
                TextStyleId = _db.GetTextStyle(decor),
                TableStyleId = _db.GetTableStyle(decor),
                DimStyleId = _db.GetDimStyle(decor),
                LeaderStyleId = _db.GetLeadersStyle(decor)
            };

            foreach (var well in wells)
            {
                rules.Build_2(well);

                var layout = new RenderLayout(cs, decorAcad, _viewFactory);
                var layoutEnts = layout.Render(well, viewType, layoutPt, out var layoutExt);
                wellsEnts.AddRange(layoutEnts);

                // Точка для следующего листа - верхний левый угол
                layoutPt = layoutExt.MinPoint.Move(0, -layoutExt.Height() - 2000);
            }

            if (!wellsEnts.Any())
            {
                MessageBox.Show("Нет элементов для отрисовки!");
                return;
            }

            // Интерактивная вставка колодца
            Insert(wellsEnts, _doc, t);
            t.Commit();
        }

        private static void Insert(List<Entity> wellEnts, Document doc, Transaction t)
        {
            var jig = new EntsJig(wellEnts, Point3d.Origin);
            var dragRes = doc.Editor.Drag(jig);
            if (dragRes.Status == PromptStatus.OK)
                return;
            wellEnts.Erase();
            throw new OperationCanceledException();
        }
    }
}