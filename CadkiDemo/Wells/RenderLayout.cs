using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.FoundationPitEntities;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Components.Decors;
using CadkiDemo.ComponentViews;
using CadkiDemo.ComponentViews.FoundationPit;
using CadkiDemo.Extension;
using CadkiDemo.Wells.Schemes;

namespace CadkiDemo.Wells
{
    /// <summary>
    /// Отрисовка листа колодца
    /// </summary>
    public class RenderLayout
    {
        private readonly Database _db;
        private readonly BlockTableRecord _owner;
        private readonly DecorOptionsAcad _decor;
        private readonly ViewFactory _viewFactory;

        public RenderLayout(BlockTableRecord owner, DecorOptionsAcad decor, ViewFactory viewFactory)
        {
            _db = owner.Database;
            _owner = owner;
            _decor = decor;
            _viewFactory = viewFactory;
        }

        public List<Entity> Render(Well well, WellViewType viewType, Point3d layoutPt, out Extents3d layoutExt)
        {
            var layoutEnts = new List<Entity>();

            // Рамка
            var sheetEnts = DrawSheet(layoutPt.Point2d(), out var sheetExt);
            layoutEnts.AddRange(sheetEnts);

            var schemePt = new Point3d(sheetExt.MinPoint.X + 1500, sheetExt.MaxPoint.Y - 500, 0);
            var schemesExt = new Extents3d();
            layoutExt = sheetExt;

            // План
            if (viewType.HasFlag(WellViewType.Plan))
            {
                var renderScheme = new SchemePlan(well, _decor, _db, _viewFactory);
                var schemeEnts = renderScheme.Render();
                MoveScheme(schemeEnts, schemePt);
                layoutEnts.AddRange(AppendToOwner(schemeEnts));

                var schemeExt = schemeEnts.Extents();
                schemesExt.AddExtents(schemeExt);
                schemePt = schemeExt.MaxPoint.Move(1000, 0);
            }

            // Сечение
            if (viewType.HasFlag(WellViewType.Section))
            {
                var renderScheme = new SchemeSection(well, _decor, _db, _viewFactory);
                var schemeEnts = renderScheme.Render();
                MoveScheme(schemeEnts, schemePt);
                layoutEnts.AddRange(AppendToOwner(schemeEnts));

                var schemeExt = schemeEnts.Extents();
                schemesExt.AddExtents(schemeExt);
            }

            // Таблица
            if (viewType.HasFlag(WellViewType.Table))
            {
                var renderScheme = new RenderTable();
                var table = renderScheme.Render(new List<Well>{ well }, _db, _decor);
                table.Position = schemesExt.MinPoint.Move(0, -1000);
                layoutEnts.AddRange(AppendToOwner(new List<Entity> {table}));
            }

            return layoutEnts;
        }

        private List<Entity> AppendToOwner(List<Entity> entities)
        {
            entities.ForEach(e => e.Append(_owner));
            return entities;
        }

        private void MoveScheme(List<Entity> ents, Point3d pt)
        {
            var ext = ents.Extents();
            var extLeftTopPt = new Point3d(ext.MinPoint.X, ext.MaxPoint.Y, 0);
            var move = pt - extLeftTopPt;
            ents.Move(move);
        }

        private List<Entity> DrawSheet(Point2d layoutPt, out Extents3d sheetExt)
        {
            var sheetEnts = new List<Entity>();

            // Внешняя рамка листа
            var width = 297 * _decor.Decor.Scale;
            var height = 210 * _decor.Decor.Scale;
            var sheetPts = new List<Point2d>
            {
                layoutPt,
                layoutPt.Move(0, height),
                layoutPt.Move(width, height),
                layoutPt.Move(width, 0)
            };

            sheetExt = new Extents3d(layoutPt.Point3d(), sheetPts[2].Point3d());
            var sheetPl = sheetPts.CreatePolyline(true);
            sheetPl.SetDecor(_decor.Decor.DecorContinuousThick, _decor.Decor.Scale, _db);
            sheetPl.Append(_owner);
            sheetEnts.Add(sheetPl);

            // Штамп
            var stampPt = sheetPts.Last().Point3d();
            InsertStamp(stampPt, sheetEnts);

            return sheetEnts;
        }

        private void InsertStamp(Point3d stampPt, List<Entity> sheetEnts)
        {
            try
            {
                var stampBtrId = _db.GetOrLoadLibBlock("Штамп_Ф3");
                var stampBlRef = _owner.InsertBlockRef(stampBtrId, stampPt, _decor.Decor.Scale);
                sheetEnts.Add(stampBlRef);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }
    }
}