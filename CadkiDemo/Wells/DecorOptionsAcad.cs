using Autodesk.AutoCAD.DatabaseServices;
using CadkiDemo.Components.Decors;

namespace CadkiDemo.Wells
{
    public class DecorOptionsAcad
    {

        public DecorOptionsAcad(DecorOptions decor)
        {
            Decor = decor;
        }

        public DecorOptions Decor { get; set; }
        public ObjectId TableStyleId { get; set; }
        public ObjectId DimStyleId { get; set; }
        public ObjectId LeaderStyleId { get; set; }
        public ObjectId TextStyleId { get; set; }
    }
}