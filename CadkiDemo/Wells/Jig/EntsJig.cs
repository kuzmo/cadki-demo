using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;

namespace CadkiDemo.Wells.Jig
{
    public class EntsJig : DrawJig
    {
        private readonly List<Entity> _ents;
        private Point3d _posPrev;
        private Point3d _pos;

        public EntsJig(List<Entity> ents, Point3d pos)
        {
            _ents = ents;
            _posPrev = pos;
            _pos = pos;
        }

        protected override SamplerStatus Sampler(JigPrompts prompts)
        {
            var res = prompts.AcquirePoint("Точка вставки");
            if (res.Status == PromptStatus.OK)
            {
                if (res.Value.DistanceTo(_pos) < 0.001)
                {
                    return SamplerStatus.NoChange;
                }

                _pos = res.Value;
                return SamplerStatus.OK;
            }

            return SamplerStatus.Cancel;
        }

        protected override bool WorldDraw(WorldDraw draw)
        {
            foreach (var entity in _ents)
            {
                var move = Matrix3d.Displacement(_pos - _posPrev);
                entity.TransformBy(move);
            }

            _posPrev = _pos;

            foreach (var entity in _ents)
            {
                if (!draw.Geometry.Draw(entity))
                    return false;
            }

            return true;
        }
    }
}