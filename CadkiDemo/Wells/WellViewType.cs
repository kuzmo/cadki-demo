﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadkiDemo.Wells
{
    /// <summary>
    /// Виды отрисовки колодца
    /// </summary>
    [Flags]
    public enum WellViewType
    {
        [Description("План")]
        Plan = 1,

        [Description("Сечение")]
        Section = 2,

        [Description("Таблица")]
        Table = 4,

        [Description("Все вместе")]
        All = 7
    }
}
