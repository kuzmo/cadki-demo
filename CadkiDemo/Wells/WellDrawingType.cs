using System.ComponentModel;

namespace CadkiDemo.Wells
{
    public enum WellDrawingType
    {
        /// <summary>
        /// Разработка грунта под котлован
        /// </summary>
        [Description("Разработка грунта под котлован")]
        Excavation,

        [Description("Обратная засыпка")]
        Backfilling
    }
}