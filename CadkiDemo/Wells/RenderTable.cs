using System.Collections.Generic;
using System.Linq;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Extension;

namespace CadkiDemo.Wells
{
    public class RenderTable
    {
        public Table Render(List<Well> wells, Database db, DecorOptionsAcad decor)
        {
            var componentsCount = wells.SelectMany(s => s.Components).GroupBy(g => g.Name).Count();
            var table = new Table();
            table.SetDatabaseDefaults(db);
            table.TableStyle = decor.TableStyleId;
            table.SetSize(3 + wells.Count, componentsCount + 1);
            table.SetBorders(headerRows: 2);

            // Название
            table.MergeCells(CellRange.Create(table, 0, 0, 0, table.Columns.Count - 1));
            var cell = table.Cells[0, 0];
            cell.SetValue("Спецификация ж/б изделий");

            var rType = 1;
            var rMark = rType + 1;
            var rFirst = rMark + 1;
            var col = table.Columns[0];
            col.Width = 25;
            col.Alignment = CellAlignment.MiddleCenter;
            cell = table.Cells[rMark, 0];
            cell.SetValue("Марка изделия");

            foreach (var column in table.Columns.Skip(1))
            {
                column.Alignment = CellAlignment.MiddleCenter;
                column.Width = 20;
            }

            foreach (var well in wells)
            {
                cell = table.Cells[rFirst, 0];
                cell.SetValue(well.Name);

                var gTypes = well.Components.GroupBy(g => g.Type);
                var c = 1;
                var r = rFirst;
                foreach (var gType in gTypes)
                {
                    cell = table.Cells[rType, c];
                    cell.SetValue(gType.Key);

                    var gNames = gType.GroupBy(g => g.Name).ToList();

                    if (gNames.Count > 1)
                    {
                        table.MergeCells(CellRange.Create(table, rType, c, rType, c + gNames.Count - 1));
                    }

                    foreach (var gName in gNames)
                    {
                        cell = table.Cells[rMark, c];
                        cell.SetValue(gName.Key);

                        cell = table.Cells[r, c];
                        var count = gName.Count();
                        cell.SetValue(count.ToString());
                        c++;
                    }
                }
            }

            var lastRow = table.Rows.Last();
            lastRow.Borders.Bottom.LineWeight = TableExtension.LwBold;

            table.TransformBy(Matrix3d.Scaling(decor.Decor.Scale, table.Position));
            return table;
        }
    }
}