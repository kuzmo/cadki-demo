using System.Collections.Generic;
using System.Linq;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.FoundationPitEntities;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Components.Decors;
using CadkiDemo.ComponentViews;
using CadkiDemo.ComponentViews.FoundationPit;
using CadkiDemo.Extension;

namespace CadkiDemo.Wells.Schemes
{
    /// <summary>
    /// Отрисовка плана колодца
    /// </summary>
    public class SchemePlan : SchemeAbstract
    {
        public SchemePlan(Well well, DecorOptionsAcad decor, Database db, ViewFactory viewFactory)
            : base(well, decor, db, viewFactory, WellViewType.Plan)
        {
        }

        protected override List<AbstractWellComponent> GetRenderedComponents(List<AbstractWellComponent> wellComponents)
        {
            var sortedComponents = wellComponents.OrderBy(x => x.Position.Z);
            return new List<AbstractWellComponent>
            {
                sortedComponents.Last()
            };
        }

        protected override List<Entity> RenderAxis(List<AbstractWellComponent> components)
        {
            var polylinePoints = components.SelectMany(c => c.GetPlanAxes().Select(x => new List<Point2d>
            {
                new Point2d(x.Start.X, x.Start.Y),
                new Point2d(x.End.X, x.End.Y)
            }));

            return polylinePoints.Select(x => (Entity) x.CreatePolyline(false)).ToList();
        }

        protected override List<Entity> RenderDim(List<AbstractWellComponent> components)
        {
            return new List<Entity>();
        }

        protected override List<Entity> RenderIsolation(List<AbstractWellComponent> components)
        {
            return new List<Entity>();
        }

        protected override List<Entity> RenderLeaders(List<AbstractWellComponent> components)
        {
            return new List<Entity>();
        }

        protected override Entity RenderSketchyPit(FoundationPit foundationPit)
        {
            var renderPit = new FoundationPit2dPlanView();
            return renderPit.Create(foundationPit, _db, Decor);
        }

        protected override List<Entity> RenderTitle(List<Entity> schemeEnts)
        {
            var ext = schemeEnts.Extents();

            var pt = new Point3d(ext.MinPoint.X + (ext.MaxPoint.X - ext.MinPoint.X) * 0.5,
                ext.MaxPoint.Y + 2 * Decor.Decor.Scale, 0);

            pt = pt.Move(0, 5 * Decor.Decor.Scale);
            var textName = TextExtension.CreateTitle("План", pt, Decor);

            return new List<Entity> {textName};
        }
    }
}