using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using CadkiDemo.Components.BusinessEntities.FoundationPitEntities;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Components.Decors;
using CadkiDemo.ComponentViews;
using CadkiDemo.Extension;

namespace CadkiDemo.Wells.Schemes
{
    /// <summary>
    /// Отрисовщик схемы - плана или сечения:
    /// Рисование компонентов, осей, размеров, выносок и доп элементов - изоляции, котлована
    /// </summary>
    public abstract class SchemeAbstract
    {
        private readonly ViewFactory _viewFactory;
        private readonly WellViewType _viewType;
        protected readonly Database _db;
        protected readonly DecorOptionsAcad Decor;
        protected readonly Well _well;

        protected SchemeAbstract(
            Well well,
            DecorOptionsAcad decor,
            Database db,
            ViewFactory viewFactory,
            WellViewType viewType)
        {
            _well = well;
            Decor = decor;
            _db = db;
            _viewFactory = viewFactory;
            _viewType = viewType;
        }

        protected abstract List<AbstractWellComponent>
            GetRenderedComponents(List<AbstractWellComponent> wellComponents);

        protected abstract List<Entity> RenderAxis(List<AbstractWellComponent> components);
        protected abstract List<Entity> RenderDim(List<AbstractWellComponent> components);
        protected abstract List<Entity> RenderLeaders(List<AbstractWellComponent> components);
        protected abstract List<Entity> RenderIsolation(List<AbstractWellComponent> components);
        protected abstract List<Entity> RenderTitle(List<Entity> schemeEnts);
        protected abstract Entity RenderSketchyPit(FoundationPit foundationPit);

        private FoundationPit GetSketchyFoundationPit()
        {
            var bottomBound = _well.Components[0].GetBound();
            var width = bottomBound.End.X - bottomBound.Start.X;
            return new FoundationPit(0, 1, width + 100, 0);
        }

        public List<Entity> Render()
        {
            var schemeEnts = new List<Entity>();

            // Компоненты для отрисовки
            var compsToRender = GetRenderedComponents(_well.Components);
            var compsRendered = new List<AbstractWellComponent>();

            foreach (var component in compsToRender)
            {
                var view = _viewFactory.GetView(component, _viewType);
                if (view == null) continue;
                var blRef = view.Create(component, _db, Decor);
                schemeEnts.Add(blRef);
                compsRendered.Add(component);
            }

            // Оси
            var axisEnts = RenderAxis(compsRendered);
            axisEnts.ForEach(x => SetDecor(x, Decor.Decor.DecorAxis));
            schemeEnts.AddRange(axisEnts);

            // Размеры
            var dimEnts = RenderDim(compsRendered);
            dimEnts.ForEach(x => SetDecor(x, Decor.Decor.DecorDim));
            schemeEnts.AddRange(dimEnts);

            // Выноски
            var leaderEnts = RenderLeaders(compsRendered);
            leaderEnts.ForEach(l => SetDecor(l, Decor.Decor.DecorLeader));
            schemeEnts.AddRange(leaderEnts);

            // Схематичный котлован
            var sketch = RenderSketchyPit(GetSketchyFoundationPit());
            schemeEnts.Add(sketch);

            // Изоляция
            var isolation = RenderIsolation(compsRendered);
            isolation.ForEach(x => SetDecor(x, Decor.Decor.DecorIsolation));
            schemeEnts.AddRange(isolation);

            // Подпись названия схемы
            var titles = RenderTitle(schemeEnts);
            titles.ForEach(x => SetDecor(x, Decor.Decor.DecorText));
            schemeEnts.AddRange(titles);

            return schemeEnts;
        }

        protected void SetDecor(Entity ent, EntityDecor decor)
        {
            ent.SetDecor(decor, Decor.Decor.Scale, _db);
        }
    }
}