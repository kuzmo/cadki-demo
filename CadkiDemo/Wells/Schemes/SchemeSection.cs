using System.Collections.Generic;
using System.Linq;
using Autodesk.AutoCAD.BoundaryRepresentation;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.FoundationPitEntities;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.ComponentViews;
using CadkiDemo.ComponentViews.FoundationPit;
using CadkiDemo.Extension;
using CadkiDemo.Utils;
using CadkiDemo.Utils.Dims;

namespace CadkiDemo.Wells.Schemes
{
    public class SchemeSection : SchemeAbstract
    {
        public SchemeSection(Well well, DecorOptionsAcad decor, Database db, ViewFactory viewFactory)
            : base(well, decor, db, viewFactory, WellViewType.Section)
        {
        }

        protected override List<AbstractWellComponent> GetRenderedComponents(List<AbstractWellComponent> wellComponents)
        {
            return wellComponents;
        }

        protected override List<Entity> RenderAxis(List<AbstractWellComponent> components)
        {
            var axis = _well.GetCentrAxis();
            var polylinePoints = axis.Select(x => new List<Point2d>() {
                new Point2d(x.Start.X, x.Start.Z),
                new Point2d(x.End.X, x.End.Z)
            });

            return polylinePoints.Select(x => (Entity)x.CreatePolyline(false)).ToList();
        }

        protected override List<Entity> RenderDim(List<AbstractWellComponent> components)
        {
            var dims = new List<Entity>();

            // Снизу - от двух нижних компонентов
            var dimPtsBot = components.Take(2)
                .SelectMany(c => c.GetDimSectionBot())
                .Select(s => s.Point2d()).ToList();
            if (dimPtsBot.Count == 0) return dims;

            var dimLineBot = new DimLineBottom();
            var dimY = dimPtsBot.Min(p => p.Y) - 500;
            var dimBotEnts = dimLineBot.GetDim(dimPtsBot, dimY, Decor.DimStyleId);
            dims.AddRange(dimBotEnts);
            return dims;
        }

        protected override List<Entity> RenderIsolation(List<AbstractWellComponent> components)
        {
            using var regions = new DisposableSet<Region>();
            components.ForEach(x =>
            {
                var points = x.GetIsolation().Points
                    .Select(x => x.Select(y => y.InvertToPoint2d()).ToList()).ToList();

                // todo определять напрпвление офсета
                var polylines = points.Select(x =>
                    x.CreatePolyline(true).GetOffsetCurves(-20.0).Cast<Polyline>().First()).ToList();

                var tempRegions = polylines.Select(x => x.ConvertToRegion()).ToList();
                regions.AddRange(tempRegions);
            });

            var unionRegion = regions.First();
            foreach (var region in regions.Skip(1))
            {
                unionRegion.BooleanOperation(BooleanOperationType.BoolUnite, region);
            }

            return unionRegion.ToPolylines()
                .Cast<Entity>()
                .ToList();
        }

        protected override List<Entity> RenderLeaders(List<AbstractWellComponent> components)
        {
            var leaders = new List<Entity>();
            var leader = new Utils.Leaders.Leader(Decor.LeaderStyleId);
            components.ForEach(comp =>
            {
                var bound = comp.GetBound();
                var middle = bound.GetMiddle();

                var p1 = new Point3d(bound.End.X, middle.Z, 0);
                var dy = comp.Position.Z < 50 ? -500 : 500;
                var p2 = p1.Move(500, dy);
                var l = leader.GetLeader(p1, p2, comp.Name);
                leaders.Add(l);
            });

            return leaders;
        }

        protected override Entity RenderSketchyPit(FoundationPit foundationPit)
        {
            var renderPit = new FoundationPit2dSectionView();
            return renderPit.Create(foundationPit, _db, Decor);
        }

        protected override List<Entity> RenderTitle(List<Entity> schemeEnts)
        {
            var ext = schemeEnts.Extents();

            var pt = new Point3d(ext.MinPoint.X + (ext.MaxPoint.X - ext.MinPoint.X) * 0.5,
                ext.MaxPoint.Y + 2 * Decor.Decor.Scale, 0);

            var textNumber = TextExtension.CreateTitle(@"{\L1-1}", pt, Decor);

            pt = pt.Move(0, 5 * Decor.Decor.Scale);
            var textName = TextExtension.CreateTitle("Поперечное сечение", pt, Decor);

            return new List<Entity> {textNumber, textName};
        }
    }
}