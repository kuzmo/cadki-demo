﻿using Autodesk.AutoCAD.BoundaryRepresentation;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadkiDemo.Extension
{
    public static class RegionExtension
    {
        public static List<Polyline> ToPolylines(this Region region)
        {
            var polylinesPointsList = new List<List<Point2d>>();
            using (Brep brp = new Brep(region))
            {
                foreach (Autodesk.AutoCAD.BoundaryRepresentation.Face f in brp.Faces)
                {
                    var temp = new List<Point2d>();
                    foreach (BoundaryLoop bl in f.Loops)
                    {
                        foreach (Autodesk.AutoCAD.BoundaryRepresentation.Vertex v in bl.Vertices)
                        {
                            temp.Add(v.Point.Point2d());
                        }

                    }
                    polylinesPointsList.Add(temp);
                }
            }
            return polylinesPointsList.Select(x => x.CreatePolyline(true)).ToList();
        }
    }
}
