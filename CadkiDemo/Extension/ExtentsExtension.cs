using System.Collections.Generic;
using System.Linq;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace CadkiDemo.Extension
{
    public static class ExtentsExtension
    {
        public static Extents3d Extents(this List<Entity> ents)
        {
            if (ents?.Any() != true)
            {
                return new Extents3d(Point3d.Origin, Point3d.Origin);
            }

            var ext = new Extents3d();
            foreach (var ent in ents.Where(w => w != null))
            {
                try
                {
                    var entExt = ent.GeometricExtents;
                    ext.AddExtents(entExt);
                }
                catch
                {
                    // Странные примитив (
                    continue;
                }
            }

            return ext;
        }

        public static double Length(this Extents3d ext)
        {
            return ext.MaxPoint.X - ext.MinPoint.X;
        }

        public static double Height(this Extents3d ext)
        {
            return ext.MaxPoint.Y - ext.MinPoint.Y;
        }
    }
}