using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using JetBrains.Annotations;

namespace CadkiDemo.Extension
{
    public static class HatchExtension
    {
        public static Hatch CreateHatch(this Polyline polyline)
        {
            var ids = new ObjectIdCollection {polyline.ObjectId};
            var result = new Hatch();
            result.AppendLoop(HatchLoopTypes.Default, ids);
            result.EvaluateHatch(true);
            return result;
        }

        /// <summary>
        /// Штриховка по двум объектам - внешний контур и внутренний
        /// </summary>
        /// <param name="outer">Внешний контур</param>
        /// <param name="inner">Внутренний контур</param>
        public static Hatch CreateHatch(this ObjectId outer, ObjectId inner)
        {
            var h = new Hatch();

            // Внешний
            var ids = new ObjectIdCollection {outer};
            h.AppendLoop(HatchLoopTypes.Default, ids);

            // Внутренний
            ids = new ObjectIdCollection {inner};
            h.AppendLoop(HatchLoopTypes.Default, ids);

            h.EvaluateHatch(false);
            return h;
        }

        [NotNull]
        public static Hatch CreateHatch(this List<Point2d> pts)
        {
            pts = pts.DistinctPoints();
            var ptCol = new Point2dCollection(pts.ToArray()) { pts[0] };
            var dCol = new DoubleCollection(new double[pts.Count]);
            var h = new Hatch();
            h.AppendLoop(HatchLoopTypes.Default, ptCol, dCol);
            h.EvaluateHatch(false);
            return h;
        }
    }
}