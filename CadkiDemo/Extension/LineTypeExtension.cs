using System.Diagnostics;
using Autodesk.AutoCAD.DatabaseServices;

namespace CadkiDemo.Extension
{
    public static class LineTypeExtension
    {
        public static ObjectId GetLineType(this Database db, string lineType, string file = "acadiso")
        {
            if (string.IsNullOrEmpty(lineType))
                return db.ContinuousLinetype;

            var ltId = GetLineTypeId(db, lineType);
            if (!ltId.IsNull) return ltId;

            try
            {
                db.LoadLineTypeFile(lineType, file);

                ltId = GetLineTypeId(db, lineType);
                if (!ltId.IsNull) return ltId;
            }
            catch
            {
                Debug.WriteLine($"Не удалось загрузить тип линии '{lineType}' из файла '{file}'.");
            }

            return db.ContinuousLinetype;
        }

        public static ObjectId GetDotLineType(this Database db)
        {
            return db.GetLineType("Штриховая");
        }

        private static ObjectId GetLineTypeId(Database db, string lineType)
        {
            using var lt =(LinetypeTable) db.LinetypeTableId.Open(OpenMode.ForRead);
            return lt.Has(lineType) ? lt[lineType] : ObjectId.Null;
        }
    }
}