using CadkiDemo.Components.BusinessEntities.WellEntities;

namespace CadkiDemo.Extension
{
    public static class ComponentExtension
    {
        public static double GetHatchAngle(this AbstractWellComponent component)
        {
            return component.IsEven ? 0 : 1.57079632679;
        }
    }
}