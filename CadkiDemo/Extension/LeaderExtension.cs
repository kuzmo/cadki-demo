using Autodesk.AutoCAD.DatabaseServices;
using CadkiDemo.Components.Decors;

namespace CadkiDemo.Extension
{
    public static class LeaderExtension
    {
        public static ObjectId GetLeadersStyle(this Database db, DecorOptions decor)
        {
            var dictStyles = db.MLeaderStyleDictionaryId.GetObjectT<DBDictionary>();
            if (dictStyles.Contains(decor.LeaderStyleName))
                return dictStyles.GetAt(decor.LeaderStyleName);

            var style = new MLeaderStyle();

            dictStyles = db.MLeaderStyleDictionaryId.GetObjectT<DBDictionary>(OpenMode.ForWrite);
            dictStyles.SetAt(decor.LeaderStyleName, style);
            db.TransactionManager.TopTransaction.AddNewlyCreatedDBObject(style, true);

            style.Name = decor.LeaderStyleName;
            style.ArrowSymbolId = DimExtension.GetArrowObjectId("DIMBLK", "_None");
            style.ContentType = ContentType.MTextContent;
            style.MaxLeaderSegmentsPoints = 2;
            style.DoglegLength = 0.5;
            style.EnableDogleg = true;
            style.TextStyleId = db.GetTextStyle(decor);
            style.TextHeight = 2.5;
            style.LandingGap = 0.2;
            style.ExtendLeaderToText = true;
            style.TextAttachmentType = TextAttachmentType.AttachmentBottomOfTopLine;
            return style.Id;
        }
    }
}