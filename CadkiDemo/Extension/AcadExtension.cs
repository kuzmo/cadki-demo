using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using JetBrains.Annotations;

namespace CadkiDemo.Extension
{
    public static class AcadExtension
    {
        public static ObjectId Append(this Entity ent, BlockTableRecord btr, Transaction? t = null)
        {
            var id = btr.AppendEntity(ent);
            if (t == null) t = btr.Database.TransactionManager.TopTransaction;
            t.AddNewlyCreatedDBObject(ent, true);
            return id;
        }

        public static void Dispose(this List<Entity> ents)
        {
            foreach (var entity in ents)
            {
                try
                {
                    entity.Dispose();
                }
                catch
                {
                    // Уже был уничтожен
                }
            }
        }

        public static void Erase(this List<Entity> ents)
        {
            foreach (var entity in ents)
            {
                try
                {
                    entity.Erase();
                }
                catch
                {
                    // Уже был уничтожен
                }
            }
        }

        public static void Move(this List<Entity> ents, Vector3d vec)
        {
            var move = Matrix3d.Displacement(vec);
            foreach (var entity in ents)
            {
                entity.TransformBy(move);
            }
        }

        [CanBeNull]
        public static T GetObject<T>(this ObjectId id, OpenMode mode = OpenMode.ForRead, Transaction? t = null)
            where T : DBObject
        {
            if (!id.IsValidEx()) return null;
            return t == null ?
                id.GetObject(mode, false, true) as T :
                t.GetObject(id, mode, false, true) as T;
        }

        [NotNull]
        public static T GetObjectT<T>(this ObjectId id, OpenMode mode = OpenMode.ForRead, Transaction t = null)
            where T : DBObject
        {
            if (!id.IsValidEx()) throw new InvalidOperationException();
            return t == null ?
                (T) id.GetObject(mode, false, true) :
                (T) t.GetObject(id, mode, false, true);
        }

        [NotNull]
        public static IEnumerable<T> GetObjects<T>([NotNull] this IEnumerable ids, OpenMode mode = OpenMode.ForRead, Transaction t = null)
            where T : DBObject
        {
            return ids
                .Cast<ObjectId>()
                .Select(id => id.GetObject<T>(mode, t))
                .Where(res => res != null);
        }

        public static bool IsValidEx(this ObjectId id)
        {
            return id.IsValid && !id.IsNull && !id.IsErased;
        }

        [NotNull]
        public static BlockTable BT(this Database db, OpenMode mode = OpenMode.ForRead, Transaction t = null)
        {
            return db.BlockTableId.GetObjectT<BlockTable>(mode, t);
        }

        [NotNull]
        public static BlockTableRecord MS(this Database db, OpenMode mode = OpenMode.ForRead, Transaction t = null)
        {
            return MsId(db).GetObjectT<BlockTableRecord>(mode, t);
        }

        [NotNull]
        public static BlockTableRecord CS(this Database db, OpenMode mode = OpenMode.ForRead, Transaction t = null)
        {
            return db.CurrentSpaceId.GetObjectT<BlockTableRecord>(mode, t);
        }

        public static ObjectId MsId(this Database db)
        {
            using var bt = (BlockTable)db.BlockTableId.Open(OpenMode.ForRead);
            return bt[BlockTableRecord.ModelSpace];
        }
    }
}