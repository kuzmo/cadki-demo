using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components;

namespace CadkiDemo.Extension
{
    public static class PositionExtension
    {
        public static Position Position(this double x, double y, double z = 0)
        {
            return new Position(x, y, z);
        }

        public static Point3d Point3d(this Position position)
        {
            return new Point3d(position.X, position.Y, position.Z);
        }

        public static Point2d InvertToPoint2d(this Position position)
        {
            return new Point2d(position.X, position.Z);
        }

        public static Point2d Point2d(this Position position)
        {
            return new Point2d(position.X, position.Y);
        }

        public static Point3d InvertToPoint3d(this Position position)
        {
            return new Point3d(position.X, position.Z, 0);
        }
    }
}