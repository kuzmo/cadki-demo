using System.Linq;
using Autodesk.AutoCAD.DatabaseServices;
using CadkiDemo.Components.Decors;
using JetBrains.Annotations;

namespace CadkiDemo.Extension
{
    public static class TableExtension
    {
        public static LineWeight LwBold => LineWeight.LineWeight040;
        public static LineWeight LwNorm => LineWeight.LineWeight015;

        public static ObjectId GetTableStyle([NotNull] this Database db,[NotNull] DecorOptions decorOptions)
        {
            var tsd = db.TableStyleDictionaryId.GetObjectT<DBDictionary>();
            if (tsd.Contains(decorOptions.TableStyleName))
                return tsd.GetAt(decorOptions.TableStyleName);

            var ts = new TableStyle();
            tsd = db.TableStyleDictionaryId.GetObjectT<DBDictionary>(OpenMode.ForWrite);
            tsd.SetAt(decorOptions.TableStyleName, ts);
            db.TransactionManager.TopTransaction.AddNewlyCreatedDBObject(ts, true);

            ts.Name = decorOptions.TableStyleName;
            var textStyleId = db.GetTextStyle(decorOptions);
            ts.SetTextStyle(textStyleId, (int)RowType.DataRow);
            ts.SetTextStyle(textStyleId, (int)RowType.HeaderRow);
            ts.SetTextStyle(textStyleId, (int)RowType.TitleRow);
            ts.HorizontalCellMargin = 1;
            ts.VerticalCellMargin = 1;

            ts.SetTextHeight(2.5, (int)RowType.DataRow);
            ts.SetTextHeight(2.5, (int)RowType.HeaderRow);
            ts.SetTextHeight(2.5, (int)RowType.TitleRow);

            return ts.Id;
        }

        public static Table SetBorders([NotNull] this Table table, bool hasTitle = true, int headerRows = 1)
        {
            var r = 0;
            if (hasTitle)
            {
                var rowTitle = table.Rows[r++];
                SetBorderTitle(rowTitle);
            }

            var rhHeight = headerRows > 1 ? 8 : 15;
            foreach (var row in table.Rows.Skip(r).Take(headerRows))
            {
                SetBorderHeader(row, rhHeight);
                r++;
            }

            foreach (var row in table.Rows.Skip(r))
            {
                SetBorderData(row);
            }

            return table;
        }

        public static Row SetBorderData([NotNull] this Row row, double? height = 8)
        {
            row.Borders.Left.LineWeight = LwBold;
            row.Borders.Right.LineWeight = LwBold;
            row.Borders.Bottom.LineWeight = LwNorm;
            row.Borders.Horizontal.LineWeight = LwNorm;
            row.Borders.Vertical.LineWeight = LwBold;
            if (height != null) row.Height = height.Value;
            return row;
        }

        public static Row SetBorderHeader([NotNull] this Row row, double? height = 15)
        {
            row.Borders.Left.LineWeight = LwBold;
            row.Borders.Right.LineWeight = LwBold;
            row.Borders.Bottom.LineWeight = LwBold;
            row.Borders.Top.LineWeight = LwBold;
            if (height != null) row.Height = height.Value;
            return row;
        }

        public static Row SetBorderTitle([NotNull] this Row row, double? height = 8)
        {
            row.Borders.Bottom.LineWeight = LwBold;
            row.Borders.Top.IsVisible = false;
            row.Borders.Left.IsVisible = false;
            row.Borders.Right.IsVisible = false;
            if (height != null) row.Height = height.Value;
            return row;
        }

        [NotNull]
        public static Cell SetValue(
            [NotNull] this Cell cell,
            string value,
            CellAlignment alignment = CellAlignment.MiddleCenter)
        {
            if (value == null) return cell;
            cell.SetValue(value, ParseOption.ParseOptionNone);
            return cell;
        }
    }
}