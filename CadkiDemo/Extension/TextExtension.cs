using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;
using CadkiDemo.Components.Decors;
using CadkiDemo.Wells;
using JetBrains.Annotations;

namespace CadkiDemo.Extension
{
    public static class TextExtension
    {
        public static ObjectId GetTextStyle([NotNull] this Database db,[NotNull] DecorOptions decorOptions)
        {
            var tst = db.TextStyleTableId.GetObjectT<TextStyleTable>();
            if (tst.Has(decorOptions.TextStyleName))
                return tst[decorOptions.TextStyleName];

            var ts = new TextStyleTableRecord
            {
                Name = decorOptions.TextStyleName,
                Font = new FontDescriptor("Arial", false, false, 0, 34),
            };

            tst = db.TextStyleTableId.GetObjectT<TextStyleTable>(OpenMode.ForWrite);
            tst.Add(ts);
            db.TransactionManager.TopTransaction.AddNewlyCreatedDBObject(ts, true);
            return ts.Id;
        }

        public static MText CreateTitle(string text, Point3d pos, DecorOptionsAcad decor)
        {
            return new MText
            {
                TextStyleId = decor.TextStyleId,
                Contents = text,
                TextHeight = 3.5 * decor.Decor.Scale,
                Location = pos,
                Attachment = AttachmentPoint.BottomCenter
            };
        }
    }
}