﻿using Autodesk.AutoCAD.Geometry;
using System.Collections.Generic;
using System.Linq;
using CadkiDemo.Utils.Comparers;
using JetBrains.Annotations;

namespace CadkiDemo.Extension
{
    public static class PointExtension
    {
        public static Point3d Point3d(this Point2d pt)
        {
            return new Point3d(pt.X, pt.Y, 0);
        }

        public static Point2d Point2d(this Point3d pt)
        {
            return new Point2d(pt.X, pt.Y);
        }

        public static Point2d Move(this Point2d pt, double x, double y)
        {
            return new Point2d(pt.X + x, pt.Y + y);
        }

        public static Point3d Move(this Point3d pt, double x, double y, double z = 0)
        {
            return new Point3d(pt.X + x, pt.Y + y, pt.Z + z);
        }

        [NotNull]
        public static List<Point2d> Close([NotNull] this List<Point2d> pts)
        {
            var pFirst = pts.First();
            var pLast = pts.Last();
            if (pFirst.IsEqualTo(pLast, Tolerance.Global)) return pts.ToList();
            var closedPts = pts.ToList();
            closedPts.Add(pFirst);
            return closedPts;
        }

        [NotNull]
        public static Point2dCollection ToCollection([NotNull] this List<Point2d> pts)
        {
            return new Point2dCollection(pts.ToArray());
        }

        [NotNull]
        public static List<Point2d> Move([NotNull] this List<Point2d> pts, double x, double y)
        {
            var lastPt = pts.Any() ? pts.Last() : Autodesk.AutoCAD.Geometry.Point2d.Origin;
            var newPt = lastPt.Move(x, y);
            pts.Add(newPt);
            return pts;
        }

        /// <summary>
        /// Отсеивание одинаковых точек
        /// </summary>
        /// <param name="points"></param>
        [NotNull]
        public static List<Point2d> DistinctPoints([NotNull] this List<Point2d> points)
        {
            // Отсеивание одинаковых точек
            return points.Distinct(new Point2dEqualityComparer()).ToList();
        }
    }
}
