using System.Windows;
using Autodesk.AutoCAD.DatabaseServices;
using CadkiDemo.Components.Decors;
using Application = Autodesk.AutoCAD.ApplicationServices.Application;

namespace CadkiDemo.Extension
{
    public static class DimExtension
    {
        public static ObjectId GetDimStyle(this Database db, DecorOptions decor)
        {
            var dst = db.DimStyleTableId.GetObjectT<DimStyleTable>();
            if (dst.Has(decor.DimStyleName))
                return dst[decor.DimStyleName];

            var dim = new DimStyleTableRecord {Name = decor.DimStyleName};
            dst = db.DimStyleTableId.GetObjectT<DimStyleTable>(OpenMode.ForWrite);
            dst.Add(dim);
            db.TransactionManager.TopTransaction.AddNewlyCreatedDBObject(dim, true);

            dim.Dimtxsty = db.GetTextStyle(decor);
            dim.Dimtxt = 2.5;
            dim.Dimdli = 3;
            dim.Dimexe = 1;
            dim.Dimexo = 3;
            dim.Dimblk = GetArrowObjectId("DIMBLK1", "_Oblique");
            dim.Dimldrblk = GetArrowObjectId("DIMLDRBLK", "_None");
            dim.Dimasz = 1.5;
            dim.Dimtad = 1;
            dim.Dimgap = 0.6;
            dim.Dimdec = 0;
            return dim.Id;
        }

        public static ObjectId GetArrowObjectId(string arrowSysVar, string arrowValue)
        {
            var doc = Application.DocumentManager.MdiActiveDocument;
            var db = doc.Database;

            var oldArrName = Application.GetSystemVariable(arrowSysVar) as string;

            // (this action may create a new block)
            Application.SetSystemVariable(arrowSysVar, arrowValue);

            // Reset the previous value
            if (oldArrName.Length != 0)
                Application.SetSystemVariable(arrowSysVar, oldArrName);

            using var bt = (BlockTable) db.BlockTableId.Open(OpenMode.ForRead);
            return bt[arrowValue];
        }
    }
}