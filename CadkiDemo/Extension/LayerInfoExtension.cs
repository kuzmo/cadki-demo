using System;
using System.Diagnostics;
using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
using CadkiDemo.Components.Decors;

namespace CadkiDemo.Extension
{
    public static class LayerInfoExtension
    {
        public static ObjectId GetLayerId(this LayerInfo layerInfo, Database db)
        {
            if (layerInfo == null) return db?.Clayer ?? ObjectId.Null;
            try
            {
                using var lt = (LayerTable) db.LayerTableId.Open(OpenMode.ForRead);

                if (lt.Has(layerInfo.Name))
                    return lt[layerInfo.Name];

                using var layer = new LayerTableRecord { Name = layerInfo.Name };
                lt.UpgradeOpen();
                lt.Add(layer);

                if (!layerInfo.Color.IsEmpty)
                {
                    layer.Color = Color.FromColor(layerInfo.Color);
                }

                if (!string.IsNullOrEmpty(layerInfo.LineType))
                {
                    layer.LinetypeObjectId = db.GetLineType(layerInfo.LineType);
                }

                if (layerInfo.LineWeight != null)
                {
                    try
                    {
                        layer.LineWeight = (LineWeight) layerInfo.LineWeight.Value;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }

                return layer.Id;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return db.Clayer;
            }
        }
    }
}