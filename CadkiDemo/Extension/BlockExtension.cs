using System;
using System.IO;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.Repositories;
using JetBrains.Annotations;

namespace CadkiDemo.Extension
{
    public static class BlockExtension
    {
        /// <summary>
        /// Загрузка библиотечного блока
        /// </summary>
        /// <param name="dbDest">Чертеж назначения</param>
        /// <param name="blName">Имя блока</param>
        /// <returns>BlockTableRecord</returns>
        public static ObjectId GetOrLoadLibBlock(this Database dbDest, string blName)
        {
            var file = new FileRepository().GetFile("Blocks.dwg");
            return GetOrLoadBlockFromFile(dbDest, file, blName);
        }

        /// <summary>
        /// Загрузка блока из файла, если его нет в текущем чертеже.
        /// </summary>
        /// <param name="dbDest">Чертеж назначения</param>
        /// <param name="file">Файл с блоком</param>
        /// <param name="blName">Имя блока</param>
        /// <param name="mode">Режим копирования</param>
        /// <returns>BlockTableRecord</returns>
        /// <exception cref="Exception">Не найден блок или файл</exception>
        public static ObjectId GetOrLoadBlockFromFile(
            this Database dbDest,
            string file,
            string blName,
            DuplicateRecordCloning mode = DuplicateRecordCloning.Ignore)
        {
            // Если блок уже есть в чертеже
            if (mode == DuplicateRecordCloning.Ignore)
            {
                var blInDest = GetBlock(dbDest, blName);
                if (!blInDest.IsNull)
                    return blInDest;
            }

            // Загрузка блока из файла
            using var dbSrc = new Database(false, true);
            dbSrc.ReadDwgFile(file, FileShare.ReadWrite, false, null);
            dbSrc.CloseInput(true);
            using var btSrc = (BlockTable) dbSrc.BlockTableId.Open(OpenMode.ForRead);
            if (!btSrc.Has(blName))
                throw new Exception($"Не найден блок '{blName}' в файле '{file}'.");
            var blIdSrc = btSrc[blName];
            using var map = new IdMapping();
            using var ids = new ObjectIdCollection {blIdSrc};
            dbDest.WblockCloneObjects(ids, dbDest.BlockTableId, map, mode, false);
            return map[blIdSrc].Value;
        }

        private static ObjectId GetBlock(Database db, string blockName)
        {
            using var btSrc = (BlockTable) db.BlockTableId.Open(OpenMode.ForRead);
            return btSrc.Has(blockName) ? btSrc[blockName] : ObjectId.Null;
        }

        /// <summary>
        /// Вставка вхождения блока
        /// </summary>
        public static BlockReference InsertBlockRef(
            this BlockTableRecord owner,
            ObjectId btrId,
            Point3d pt,
            double scale = 1)
        {
            var blRef = new BlockReference(pt, btrId);
            blRef.Append(owner);

            if (Math.Abs(scale - 1) > 0.01)
                blRef.TransformBy(Matrix3d.Scaling(scale, pt));

            AddAttributes(blRef);
            return blRef;
        }

        /// <summary>
        /// Добавление атрибутов к вставке блока
        /// </summary>
        public static void AddAttributes(this BlockReference blRef)
        {
            var t = blRef.BlockTableRecord.Database.TransactionManager.TopTransaction;
            foreach (var atrDef in blRef.BlockTableRecord
                .GetObjectT<BlockTableRecord>().GetObjects<AttributeDefinition>())
            {
                if (atrDef.Constant) continue;
                using var atrRef = new AttributeReference();
                atrRef.SetAttributeFromBlock(atrDef, blRef.BlockTransform);
                blRef.AttributeCollection.AppendAttribute(atrRef);
                t.AddNewlyCreatedDBObject(atrRef, true);
            }
        }
    }
}