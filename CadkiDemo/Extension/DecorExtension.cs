using System;
using System.Diagnostics;
using System.Drawing;
using Autodesk.AutoCAD.DatabaseServices;
using CadkiDemo.Components.Decors;
using JetBrains.Annotations;
using HatchPatternType = Autodesk.AutoCAD.DatabaseServices.HatchPatternType;

namespace CadkiDemo.Extension
{
    public static class DecorExtension
    {
        public static void SetDecor(this Entity ent, EntityDecor decor, double scale, Database db)
        {
            if (decor == null || ent == null) return;
            SetEntityDecor(ent, decor, scale, db);

            switch (ent)
            {
                case Hatch h:
                    if (decor is HatchDecor hatchDecor)
                        SetHatchDecor(h, hatchDecor, scale);
                    break;
                case Dimension dim:
                    dim.Dimscale = scale;
                    break;
                case MLeader mLeader:
                    mLeader.Scale = scale;
                    break;
            }
        }

        private static void SetHatchDecor([NotNull] Hatch hatch,[NotNull] HatchDecor decor, double scale)
        {
            var update = false;

            if (scale > 0)
            {
                hatch.PatternScale = scale * decor.PatternScaleFactor;
                update = true;
            }

            if (!string.IsNullOrEmpty(decor.PatternName))
            {
                hatch.SetHatchPattern((HatchPatternType) decor.PatternType, decor.PatternName);
                update = true;
            }

            if (update)
            {
                hatch.EvaluateHatch(true);
            }
        }

        private static void SetEntityDecor([NotNull] Entity ent,[NotNull] EntityDecor decor, double scale, Database db)
        {
            SetLayer(ent, decor.Layer, db);
            SetColor(ent, decor.Color);
            SetScale(ent, scale);
            SetLineWeight(ent, decor.LineWeight);
            SetLineType(ent, decor.LineType, db);
        }

        private static void SetColor(Entity ent, Color color)
        {
            if (color.IsEmpty)
            {
                ent.ColorIndex = 256;
            }
            else
            {
                ent.Color = Autodesk.AutoCAD.Colors.Color.FromColor(color);
            }
        }

        private static void SetLayer(Entity ent, LayerInfo layerInfo, Database db)
        {
            var layId = layerInfo.GetLayerId(db);
            if (layId.IsNull) return;
            ent.LayerId = layId;
        }

        private static void SetLineType(Entity ent, string lineType, Database db)
        {
            if (string.IsNullOrEmpty(lineType))
            {
                ent.Linetype = SymbolUtilityServices.LinetypeByLayerName;
                return;
            }

            ent.LinetypeId = db.GetLineType(lineType);
        }

        private static void SetLineWeight(Entity ent, int? lw)
        {
            if (lw == null)
            {
                ent.LineWeight = LineWeight.ByLayer;
                return;
            }

            try
            {
                ent.LineWeight = (LineWeight) lw.Value;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private static void SetScale(Entity ent, double scale)
        {
            if (scale > 0) ent.LinetypeScale = scale;
        }
    }
}