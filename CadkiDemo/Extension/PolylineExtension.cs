﻿using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using JetBrains.Annotations;

namespace CadkiDemo.Extension
{
    public static class PolylineExtension
    {
        public static double Tolerance = 0.001;

        public static Polyline CreatePolyline(this List<Point2d> points, bool isClose)
        {
            var result = new Polyline(points.Count);
            for (int i = 0; i < points.Count; i++)
            {
                result.AddVertexAt(i, points[i], 0, 0, 0);
            }

            result.Closed = isClose;
            return result;
        }

        public static bool IsEqual(this Polyline currentPoly, Polyline polyline)
        {
            var currentPointsCount = currentPoly.NumberOfVertices;
            var pointsCount = polyline.NumberOfVertices;

            if (currentPointsCount != pointsCount) return false;

            // todo restruct list of points if start points not equals
            for (int i = 0; i < pointsCount; i++)
            {
                var point1 = currentPoly.GetPoint2dAt(i);
                var point2 = polyline.GetPoint2dAt(i);
                if (Math.Abs(point1.X - point2.X) >= Tolerance ||
                    Math.Abs(point1.Y - point2.Y) >= Tolerance) return false;
            }
            return true;
        }

        [NotNull]
        public static List<Point2d> GetPoints([NotNull] this Polyline pl)
        {
            var points = new List<Point2d>();
            for (var i = 0; i < pl.NumberOfVertices; i++)
            {
                points.Add(pl.GetPoint2dAt(i));
            }

            return points;
        }

        [NotNull]
        public static Region ConvertToRegion([NotNull] this Polyline pl)
        {
            var acDBObjColl = new DBObjectCollection();
            acDBObjColl.Add(pl);
            var regionColl = new DBObjectCollection();
            regionColl = Region.CreateFromCurves(acDBObjColl);
            return regionColl[0] as Region;
        }

        public static Polyline Rectangle(this Point2d center, double length, double height)
        {
            var rec = new Polyline(4);
            var lHalf = length * 0.5;
            var hHalf = height * 0.5;
            rec.AddVertexAt(0, center.Move(-lHalf, -hHalf), 0, 0, 0);
            rec.AddVertexAt(0, center.Move(-lHalf, hHalf), 0, 0, 0);
            rec.AddVertexAt(0, center.Move(lHalf, hHalf), 0, 0, 0);
            rec.AddVertexAt(0, center.Move(lHalf, -hHalf), 0, 0, 0);
            rec.Closed = true;
            return rec;
        }

        public static Polyline Rectangle(this Point3d center, double length, double height)
        {
            return center.Point2d().Rectangle(length, height);
        }
    }
}
