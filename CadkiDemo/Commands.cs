﻿using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Windows;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Components.Decors;
using CadkiDemo.Wells;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using UI;
using UI.ViewModels;

namespace CadkiDemo
{
    public class Commands
    {
        public Main MainModel = new Main();

        [CommandMethod("test")]
        public void Test()
        {
            MainModel.OnDraw += this.TestDraw;
            var cadKiControl = new UI.Views.DemoControl(MainModel);
            var ps = new PaletteSet("CadKi");

            ps.Size = new Size(340, 300);
            ps.DockEnabled = (DockSides)((int)DockSides.Left + (int)DockSides.Right);

            ElementHost host = new ElementHost();
            host.AutoSize = true;
            host.Dock = DockStyle.Fill;
            host.Child = cadKiControl;
            

            ps.Add("Add", host);


            ps.KeepFocus = true;
            ps.Visible = true;
        }

        private async void TestDraw(OrderVM orderVM)
        {
            try
            {
                if (orderVM.InstanceId == null) return;
                var lst = await MainModel.GetWellData(orderVM.InstanceId);
                var wells = new List<Well>
                {
                    new Well(lst)
                };
                var wellModel = new RenderWells();
                wellModel.DrawWells(wells, WellViewType.All, new DecorOptions());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        [CommandMethod("draw")]
        public void Draw()
        {
            try
            {
                var wellModel = new RenderWells();
                var wells = new List<Well>
                {
                    new Well(new List<AbstractWellComponent>()
                    {
                        new Ring
                        {
                            Thickness = 80,
                            Diametr = 1160,
                            Height = 590,
                            Name = "КС10.6",
                            GOST = "ГОСТ 8020-90"
                        }
                    })

                };

                wellModel.DrawWells(wells, WellViewType.Section, new DecorOptions());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
    }
}
