﻿using Autodesk.AutoCAD.DatabaseServices;
using CadkiDemo.Components.BusinessEntities.Abstract;
using CadkiDemo.Wells;

namespace CadkiDemo.ComponentViews
{
    public interface IComponentView
    {
        BlockReference Create(AbstractDrawComponent component, Database db, DecorOptionsAcad decor);
    }
}