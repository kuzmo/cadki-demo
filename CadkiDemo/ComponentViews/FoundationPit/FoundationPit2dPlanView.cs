﻿using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Extension;

namespace CadkiDemo.ComponentViews.FoundationPit
{
    public class FoundationPit2dPlanView : ComponentViewAbstract<Components.BusinessEntities.FoundationPitEntities.FoundationPit>
    {
        private List<Point2d> _outerContourPts = new List<Point2d>();
        private List<Point2d> _innerContourPts = new List<Point2d>();
        private readonly List<List<Point2d>> _polylinesPts = new List<List<Point2d>>();
        protected override void Draw(Components.BusinessEntities.FoundationPitEntities.FoundationPit foundationPit)
        {
            CreateGeometry(foundationPit);

            // Контуры котлована
            var plPit = _outerContourPts.CreatePolyline(true);
            AppendThin(plPit);

            plPit = _innerContourPts.CreatePolyline(true);
            AppendDot(plPit);

            foreach (var plPts in _polylinesPts)
            {
                plPit = plPts.CreatePolyline(false);
                AppendThin(plPit);
            }
        }

        private void CreateGeometry(Components.BusinessEntities.FoundationPitEntities.FoundationPit pitFoundation)
        {
            var leftDx = pitFoundation.Height / Math.Tan(pitFoundation.LeftSkew);
            var rightDx = pitFoundation.Height / Math.Tan(pitFoundation.RightSkew);
            var halfBottomWidth = pitFoundation.BottomWidth / 2;

            // внутренний контур
            _innerContourPts = new List<Point2d>
            {
                new Point2d(-halfBottomWidth, halfBottomWidth),
                new Point2d(-halfBottomWidth, -halfBottomWidth),
                new Point2d(halfBottomWidth, -halfBottomWidth),
                new Point2d(halfBottomWidth, halfBottomWidth),
            };

            // внешний контур
            _outerContourPts = new List<Point2d>
            {
                new Point2d(-halfBottomWidth - leftDx, halfBottomWidth + leftDx),
                new Point2d(-halfBottomWidth - leftDx, -halfBottomWidth - leftDx),
                new Point2d(halfBottomWidth + rightDx, -halfBottomWidth - rightDx),
                new Point2d(halfBottomWidth + rightDx, halfBottomWidth + rightDx),
            };

            // уголки :)
            for (int i = 0; i < 4; i++)
            {
                _polylinesPts.Add(new List<Point2d> { _innerContourPts[i], _outerContourPts[i] });
            }

            // уклон
            var l = pitFoundation.BottomWidth + leftDx + rightDx;
            var k = 300;
            var a = 1;
            while (k * a < l)
            {
                if (a % 2 == 0)
                {
                    _polylinesPts.Add(GetSkewXLine(-halfBottomWidth - leftDx, -halfBottomWidth - leftDx + k * a, 300));
                    _polylinesPts.Add(GetSkewXLine(halfBottomWidth + rightDx, halfBottomWidth + rightDx - k * a, -300));
                    _polylinesPts.Add(GetSkewYLine(-halfBottomWidth - leftDx + k * a, -halfBottomWidth - leftDx, 300));
                    _polylinesPts.Add(GetSkewYLine(halfBottomWidth + rightDx - k * a, halfBottomWidth + rightDx, -300));
                }
                else
                {
                    _polylinesPts.Add(GetSkewXLine(-halfBottomWidth - leftDx, -halfBottomWidth - leftDx + k * a, 100));
                    _polylinesPts.Add(GetSkewXLine(halfBottomWidth + rightDx, halfBottomWidth + rightDx - k * a, -100));
                    _polylinesPts.Add(GetSkewYLine(-halfBottomWidth - leftDx + k * a, -halfBottomWidth - leftDx, 100));
                    _polylinesPts.Add(GetSkewYLine(halfBottomWidth + rightDx - k * a, halfBottomWidth + rightDx, -100));
                }
                a += 1;
            }
        }

        private List<Point2d> GetSkewXLine(double x, double y, double length)
        {
            return new List<Point2d> { new Point2d(x, y), new Point2d(x + length, y) };
        }

        private List<Point2d> GetSkewYLine(double x, double y, double length)
        {
            return new List<Point2d> { new Point2d(x, y), new Point2d(x, y + length) };
        }

    }
}
