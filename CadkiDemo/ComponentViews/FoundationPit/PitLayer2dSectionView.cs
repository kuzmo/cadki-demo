﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.FoundationPitEntities;
using CadkiDemo.Extension;

namespace CadkiDemo.ComponentViews.Section
{
    public class PitLayer2dSectionView : SectionComponentViewAbstract<PitLayer>
    {
        private List<Point2d> _closedPolylinePts;
        private List<Point3d> _pointsForHatches;
        private string _hatchStyle;
        private readonly Editor _ed;

        protected override void Draw(PitLayer pitLayer)
        {
            CreateGeometry(pitLayer);

            var clPl = _closedPolylinePts.CreatePolyline(true);
            AppendThick(clPl);

            var polylinesForHatches = new List<Polyline>();
            _pointsForHatches.ForEach(x =>
            {
                // TODO: не будет работать GetPolylineBoundaryByPoint - т.к. рисуется все в блоке, не в Модели!
                var r = GetPolylineBoundaryByPoint(x);
                if (r.Count > 0)
                {
                    var isPolylineExist = polylinesForHatches.Any(y => y.IsEqual(r[0]));
                    if (!isPolylineExist) polylinesForHatches.Add(r[0]);
                }
            });

            _ed.Regen();
            polylinesForHatches.ForEach(x =>
            {
                AppendThin(x);

                var hatch = x.CreateHatch();
                AppendHatchConcrete(hatch);
                hatch.SetHatchPattern(HatchPatternType.UserDefined, _hatchStyle);
            });

            var polylineForDelete = clPl.Id.GetObject(OpenMode.ForWrite);
            polylineForDelete.Erase(true);
        }

        private void CreateGeometry(PitLayer pitLayer)
        {
            var leftDx = pitLayer.Height / Math.Tan(pitLayer.LeftSkew);
            var rightDx = pitLayer.Height / Math.Tan(pitLayer.RightSkew);
            _closedPolylinePts = new List<Point2d>
            {
                new Point2d(),
                new Point2d(pitLayer.BottomWidth, 0),
                new Point2d(pitLayer.BottomWidth + rightDx, pitLayer.Height),
                new Point2d(-leftDx , pitLayer.Height)
            };

            _pointsForHatches = new List<Point3d>
            {
                new Point3d(0, pitLayer.Height / 2, 0),
                new Point3d(pitLayer.BottomWidth, pitLayer.Height / 2, 0)
            };

            switch (pitLayer.LayerType)
            {
                case LayerType.CrushedStone:
                    _hatchStyle = "GRAVEL";
                    break;
                case LayerType.Sand:
                    _hatchStyle = "AR-SAND";
                    break;
                case LayerType.Soil:
                    _hatchStyle = "TRIANG";
                    break;
                default:
                    _hatchStyle = "ANSI31";
                    break;
            }
        }

        public List<Polyline> GetPolylineBoundaryByPoint(Point3d point)
        {
            var result = new List<Polyline>();
            var collection = _ed.TraceBoundary(point, true);
            foreach (DBObject obj in collection)
            {
                var poly = obj as Polyline;
                if (poly != null)
                {
                    result.Add(poly);
                }
            }
            return result;
        }
    }
}
