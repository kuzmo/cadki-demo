﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Extension;
using CadkiDemo.Utils;

namespace CadkiDemo.ComponentViews.FoundationPit
{
    /// <summary>
    /// Вид сечения котлована
    /// </summary>
    public class FoundationPit2dSectionView : ComponentViewAbstract<Components.BusinessEntities.FoundationPitEntities.FoundationPit>
    {
        private readonly List<List<Point2d>> _polylinesPts = new List<List<Point2d>>();

        protected override void Draw(Components.BusinessEntities.FoundationPitEntities.FoundationPit foundationPit)
        {
            CreateGeometry(foundationPit);
            foreach (var plPts in _polylinesPts)
            {
                // Контур котлована
                var plPit = plPts.CreatePolyline(false);
                AppendThick(plPit);

                // Штриховка земли по контуру
                AppendEarth(plPit);
            }
        }

        private void AppendEarth(Polyline plPit)
        {
            var pitPts = plPit.GetPoints();
            using var offsets = new DisposableSet<Polyline>(plPit.GetOffsetCurves(2.25 * _decor.Decor.Scale).Cast<Polyline>());
            var offset = offsets.First();
            var offsetPts = offset.GetPoints();

            var hatchPts = (pitPts.Last() - offsetPts.Last()).Length < (pitPts.Last() - offsetPts.First()).Length
                ? pitPts.Concat(Enumerable.Reverse(offsetPts)).ToList()
                : pitPts.Concat(offsetPts).ToList();

            var h = hatchPts.CreateHatch();
            AppendHatchEarth(h, Math.PI / 4);
        }

        private void CreateGeometry(Components.BusinessEntities.FoundationPitEntities.FoundationPit pitFoundation)
        {
            var leftDx = pitFoundation.Height / Math.Tan(pitFoundation.LeftSkew);
            var rightDx = pitFoundation.Height / Math.Tan(pitFoundation.RightSkew);
            _polylinesPts.Add(new List<Point2d>
            {
                new Point2d(0.0 - pitFoundation.BottomWidth / 2 - leftDx, pitFoundation.Height),
                new Point2d(-pitFoundation.BottomWidth / 2, 0.0),
                new Point2d(pitFoundation.BottomWidth / 2, 0.0),
                new Point2d(pitFoundation.BottomWidth / 2 + rightDx, pitFoundation.Height),
            });
        }
    }
}
