﻿using System.Collections.Generic;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Extension;

namespace CadkiDemo.ComponentViews.Section
{
    public class WorkingChamber2dSectionView : SectionComponentViewAbstract<WorkingChamber>
    {
        private readonly List<List<Point2d>> _polylinesPts = new List<List<Point2d>>();
        private readonly List<List<Point2d>> _closedPolylinesPts = new List<List<Point2d>>();

        protected override void Draw(WorkingChamber workingChamber)
        {
            CreateGeometry(workingChamber);

            foreach (var closedPolylinePts in _closedPolylinesPts)
            {
                var clPl = closedPolylinePts.CreatePolyline(true);
                AppendThick(clPl);

                var h = clPl.CreateHatch();
                AppendHatchConcrete(h, workingChamber.GetHatchAngle());
            }

            foreach (var polylinePts in _polylinesPts)
            {
                var pl = polylinePts.CreatePolyline(false);
                AppendThick(pl);
            }
        }

        private void CreateGeometry(WorkingChamber workingChamber)
        {
            var radius = workingChamber.Diametr / 2;
            var innerRadius = (workingChamber.Diametr - workingChamber.Thickness * 2) / 2;
            _closedPolylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(-radius, 0),
                    new Point2d(-radius + workingChamber.Thickness, 0),
                    new Point2d(-radius + workingChamber.Thickness, workingChamber.Height),
                    new Point2d(-radius, workingChamber.Height)
                });
            _closedPolylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(radius - workingChamber.Thickness, 0),
                    new Point2d(radius, 0),
                    new Point2d(radius, workingChamber.Height),
                    new Point2d(radius - workingChamber.Thickness, workingChamber.Height)
                });
            _closedPolylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(-innerRadius, 0),
                    new Point2d(innerRadius, 0),
                    new Point2d(innerRadius, workingChamber.BottomThickness),
                    new Point2d(-innerRadius, workingChamber.BottomThickness)
                });
            _polylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(-radius, 0),
                    new Point2d(radius, 0)
                });
            _polylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(-radius, workingChamber.Height),
                    new Point2d(radius, workingChamber.Height)
                });
        }
    }
}
