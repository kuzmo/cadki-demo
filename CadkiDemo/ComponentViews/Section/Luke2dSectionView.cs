﻿using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Extension;
using System.Collections.Generic;

namespace CadkiDemo.ComponentViews.Section
{
    public class Luke2dSectionView : SectionComponentViewAbstract<Luke>
    {
        protected override void Draw(Luke component)
        {
            var polylinePoints = CreateGeometry(component);
            var clPl = polylinePoints.CreatePolyline(true);
            AppendThick(clPl);
            var h = clPl.CreateHatch();
            AppendHatchConcrete(h, component.GetHatchAngle());
        }

        private List<Point2d> CreateGeometry(Luke luke)
        {
            var radius = luke.Diametr / 2;
            return new List<Point2d> {
                new Point2d(-radius, 0),
                new Point2d(-radius, luke.Height),
                new Point2d(radius, luke.Height),
                new Point2d(radius, 0)
            };   
        }
    }
}
