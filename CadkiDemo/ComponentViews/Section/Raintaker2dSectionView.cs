﻿using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Extension;
using System.Collections.Generic;

namespace CadkiDemo.ComponentViews.Section
{
    public class Raintaker2dSectionView : SectionComponentViewAbstract<Raintaker>
    {
        protected override void Draw(Raintaker component)
        {
            var polylinePoints = CreateGeometry(component);
            var clPl = polylinePoints.CreatePolyline(true);
            AppendThick(clPl);
            var h = clPl.CreateHatch();
            AppendHatchConcrete(h, component.GetHatchAngle());
        }

        private List<Point2d> CreateGeometry(Raintaker component)
        {
            var halfLength = component.Length/ 2;
            return new List<Point2d> {
                new Point2d(-halfLength, 0),
                new Point2d(-halfLength, component.Height),
                new Point2d(halfLength, component.Height),
                new Point2d(halfLength, 0)
            };
        }
    }
}
