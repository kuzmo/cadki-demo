﻿using System.Collections.Generic;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Extension;

namespace CadkiDemo.ComponentViews.Section
{
    public class BasePlate2dSectionView : SectionComponentViewAbstract<BasePlate>
    {
        private readonly List<List<Point2d>> _closedPolylinesPts = new List<List<Point2d>>();
        private readonly List<List<Point2d>> _polylinesPts = new List<List<Point2d>>();

        protected override void Draw(BasePlate basePlate)
        {
            CreateGeometry(basePlate);
            var hatchAngle = basePlate.GetHatchAngle();

            foreach (var closedPolylinePts in _closedPolylinesPts)
            {
                var pl = closedPolylinePts.CreatePolyline(true);
                AppendThick(pl);

                var h = pl.CreateHatch();
                AppendHatchConcrete(h, hatchAngle);
            }

            foreach (var polylinePts in _polylinesPts)
            {
                var poly = polylinePts.CreatePolyline(false);
                AppendThick(poly);
            }
        }

        private void CreateGeometry(BasePlate basePlate)
        {
            var plateRadius = basePlate.Length / 2;
            var holeRadius = basePlate.Diametr / 2;
            _closedPolylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(-plateRadius, 0),
                    new Point2d(-holeRadius, 0),
                    new Point2d(-holeRadius, basePlate.Height),
                    new Point2d(-plateRadius, basePlate.Height)
                });
            _closedPolylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(holeRadius, 0),
                    new Point2d(plateRadius, 0),
                    new Point2d(plateRadius, basePlate.Height),
                    new Point2d(holeRadius, basePlate.Height)
                });
            _polylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(-holeRadius, 0),
                    new Point2d(holeRadius, 0)
                });
            _polylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(-holeRadius, basePlate.Height),
                    new Point2d( holeRadius, basePlate.Height)
                });
        }
    }
}
