﻿using System.Collections.Generic;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.WellEntities;

using CadkiDemo.Extension;

namespace CadkiDemo.ComponentViews.Section
{
    public class BottomPlate2dSectionView : SectionComponentViewAbstract<BottomPlate>
    {
        private readonly List<List<Point2d>> _closedPolylinesPts = new List<List<Point2d>>();

        protected override void Draw(BottomPlate plate)
        {
            CreateGeometry(plate);

            foreach (var closedPolylinePts in _closedPolylinesPts)
            {
                var pl = closedPolylinePts.CreatePolyline(true);
                AppendThick(pl);

                var h = pl.CreateHatch();
                AppendHatchConcrete(h);
            }
        }

        private void CreateGeometry(BottomPlate plate)
        {
            var radius = plate.Diametr / 2;
            _closedPolylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(-radius, 0),
                    new Point2d(radius, 0),
                    new Point2d(radius, plate.Height),
                    new Point2d(-radius, plate.Height)
                });
        }
    }
}
