﻿using System.Collections.Generic;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Extension;

namespace CadkiDemo.ComponentViews.Section
{
    public class FloorSlab2dSectionView : SectionComponentViewAbstract<FloorSlab>
    {
        private readonly List<List<Point2d>> _closedPolylinesPts = new List<List<Point2d>>();
        private readonly List<List<Point2d>> _polylinesPts = new List<List<Point2d>>();

        protected override void Draw(FloorSlab floorSlab)
        {
            CreateGeometry(floorSlab);
            var hatchAngle = floorSlab.GetHatchAngle();

            foreach (var closedPolylinePts in _closedPolylinesPts)
            {
                var pl = closedPolylinePts.CreatePolyline(true);
                AppendThick(pl);

                var h = pl.CreateHatch();
                AppendHatchConcrete(h, hatchAngle);
            }

            foreach (var polylinesPts in _polylinesPts)
            {
                var pl = polylinesPts.CreatePolyline(false);
                AppendThick(pl);
            }
        }

        private void CreateGeometry (FloorSlab floorSlab)
        {
            var plateRadius = floorSlab.Diametr / 2;
            var holeStart = floorSlab.HoleOffset - floorSlab.HoleDiametr / 2;
            var holeEnd = floorSlab.HoleOffset + floorSlab.HoleDiametr / 2;
            _closedPolylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(- plateRadius, 0),
                    new Point2d(+ holeStart, 0),
                    new Point2d(+ holeStart, floorSlab.Height),
                    new Point2d(- plateRadius, floorSlab.Height)
                });
            _closedPolylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(holeEnd, 0),
                    new Point2d(plateRadius, 0),
                    new Point2d(plateRadius, floorSlab.Height),
                    new Point2d(holeEnd, floorSlab.Height)
                });
            _polylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(holeStart, 0),
                    new Point2d(holeEnd, 0)
                });
            _polylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(holeStart, floorSlab.Height),
                    new Point2d(holeEnd, floorSlab.Height)
                });
        }
    }
}
