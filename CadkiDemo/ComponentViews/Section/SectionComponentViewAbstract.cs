using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.Abstract;

namespace CadkiDemo.ComponentViews.Section
{
    public abstract class SectionComponentViewAbstract<T> : ComponentViewAbstract<T> where T : AbstractDrawComponent
    {
        protected override Point3d GetPosition(AbstractDrawComponent component)
        {
            return new Point3d(component.Position.X, component.Position.Z, 0);
        }
    }
}