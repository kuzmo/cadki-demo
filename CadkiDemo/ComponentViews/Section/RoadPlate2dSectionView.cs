﻿using System.Collections.Generic;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Extension;

namespace CadkiDemo.ComponentViews.Section
{
    public class RoadPlate2dSectionView : SectionComponentViewAbstract<RoadPlate>
    {
        private readonly List<List<Point2d>> _closedPolylinesPts = new List<List<Point2d>>();
        private readonly List<List<Point2d>> _polylinesPts = new List<List<Point2d>>();

        protected override void Draw(RoadPlate roadPlate)
        {
            CreateGeometry(roadPlate);

            foreach (var closedPolylinePts in _closedPolylinesPts)
            {
                var clPl = closedPolylinePts.CreatePolyline(true);
                AppendThick(clPl);

                var h = clPl.CreateHatch();
                AppendHatchConcrete(h, roadPlate.GetHatchAngle());
            }

            foreach (var polylinePts in _polylinesPts)
            {
                var pl = polylinePts.CreatePolyline(false);
                AppendThick(pl);
            }
        }

        private void CreateGeometry (RoadPlate roadPlate)
        {
            var plateRadius = roadPlate.Length / 2;
            var holeRadius = roadPlate.HoleDiametr / 2;
            _closedPolylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(-plateRadius, 0),
                    new Point2d(-holeRadius, 0),
                    new Point2d(-holeRadius, roadPlate.Height),
                    new Point2d(-plateRadius, roadPlate.Height)
                });
            _closedPolylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(holeRadius, 0),
                    new Point2d(plateRadius, 0),
                    new Point2d(plateRadius, roadPlate.Height),
                    new Point2d(holeRadius, roadPlate.Height)
                });
            _polylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(-holeRadius, 0),
                    new Point2d(holeRadius, 0)
                });
            _polylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(-holeRadius, roadPlate.Height),
                    new Point2d(holeRadius, roadPlate.Height)
                });
        }
    }
}
