﻿using System.Collections.Generic;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Extension;

namespace CadkiDemo.ComponentViews.Section
{
    public class Ring2dSectionView : SectionComponentViewAbstract<Ring>
    {
        private readonly List<List<Point2d>> _polylinesPts = new List<List<Point2d>>();
        private readonly List<List<Point2d>> _closedPolylinesPts = new List<List<Point2d>>();

        protected override void Draw(Ring ring)
        {
            CreateGeometry(ring);

            foreach (var closedPolylinePts in _closedPolylinesPts)
            {
                var clPl = closedPolylinePts.CreatePolyline(true);
                AppendThick(clPl);

                var h = clPl.CreateHatch();
                AppendHatchConcrete(h, ring.GetHatchAngle());
            }

            foreach (var polylinePts in _polylinesPts)
            {
                var pl = polylinePts.CreatePolyline(false);
                AppendThick(pl);
            }
        }

        private void CreateGeometry(Ring ring)
        {
            var radius = ring.Diametr / 2;
            _closedPolylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(-radius, 0),
                    new Point2d(-radius + ring.Thickness, 0),
                    new Point2d(-radius + ring.Thickness, ring.Height),
                    new Point2d(-radius, ring.Height)
                });
            _closedPolylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(radius - ring.Thickness, 0),
                    new Point2d(radius, 0),
                    new Point2d(radius, ring.Height),
                    new Point2d(radius - ring.Thickness, ring.Height)
                });
            _polylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(-radius, 0),
                    new Point2d(radius, 0)
                });
            _polylinesPts.Add(
                new List<Point2d>
                {
                    new Point2d(-radius, ring.Height),
                    new Point2d(radius, ring.Height)
                });
        }
    }
}
