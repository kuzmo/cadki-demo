using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.Abstract;

namespace CadkiDemo.ComponentViews.Blocks
{
    public class BlockComponentPlanView : BlockComponentViewAbstract
    {
        public BlockComponentPlanView(string blockFile) : base(blockFile)
        {
        }

        protected override string GetBlockName(AbstractDrawComponent component)
        {
            return $"{component.Name}_план";
        }

        protected override Point3d GetPosition(AbstractDrawComponent component)
        {
            return new Point3d(component.Position.X, component.Position.Y, 0);
        }
    }
}