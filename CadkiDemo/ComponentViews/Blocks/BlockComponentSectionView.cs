using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components;
using CadkiDemo.Components.BusinessEntities.Abstract;

namespace CadkiDemo.ComponentViews.Blocks
{
    public class BlockComponentSectionView : BlockComponentViewAbstract
    {
        public BlockComponentSectionView(string blockFile) : base(blockFile)
        {
        }

        protected override string GetBlockName(AbstractDrawComponent component)
        {
            return $"{component.Name}_разрез";
        }

        protected override Point3d GetPosition(AbstractDrawComponent component)
        {
            return new Point3d(component.Position.X, component.Position.Z, 0);
        }
    }
}