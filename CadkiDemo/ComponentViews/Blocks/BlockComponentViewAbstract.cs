using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.Abstract;
using CadkiDemo.Extension;
using CadkiDemo.Wells;

namespace CadkiDemo.ComponentViews.Blocks
{
    public abstract class BlockComponentViewAbstract : IComponentView
    {
        private readonly string _blockFile;

        protected BlockComponentViewAbstract(string blockFile)
        {
            _blockFile = blockFile;
        }

        protected abstract string GetBlockName(AbstractDrawComponent component);
        protected abstract Point3d GetPosition(AbstractDrawComponent component);

        public BlockReference Create(AbstractDrawComponent component, Database db, DecorOptionsAcad decor)
        {
            var blName = GetBlockName(component);
            var btrId = db.GetOrLoadBlockFromFile(_blockFile, blName, DuplicateRecordCloning.Replace);
            return new BlockReference(GetPosition(component), btrId);
        }
    }
}