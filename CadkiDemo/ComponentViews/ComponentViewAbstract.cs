using System;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.Abstract;
using CadkiDemo.Components.Decors;
using CadkiDemo.Extension;
using CadkiDemo.Wells;

namespace CadkiDemo.ComponentViews
{
    public abstract class ComponentViewAbstract<T> : IComponentView where T : AbstractDrawComponent
    {
        protected Database _db;
        protected DecorOptionsAcad _decor;
        private BlockTableRecord _compBtr;

        protected abstract void Draw(T component);

        protected virtual Point3d GetPosition(AbstractDrawComponent component)
        {
            return component.Position.Point3d();
        }

        public BlockReference Create(AbstractDrawComponent component, Database db, DecorOptionsAcad decor)
        {
            _db = db;
            _decor = decor;

            var name = $"{component.Name}_{Guid.NewGuid()}";
            _compBtr = new BlockTableRecord {Name = name};
            _db.BlockTableId.GetObjectT<BlockTable>(OpenMode.ForWrite).Add(_compBtr);
            _db.TransactionManager.TopTransaction.AddNewlyCreatedDBObject(_compBtr, true);

            Draw((T) component);

            var compPt = GetPosition(component);
            return new BlockReference(compPt, _compBtr.Id);
        }

        protected void Append(Entity draw, EntityDecor decor)
        {
            draw.Append(_compBtr);
            draw.SetDecor(decor, _decor.Decor.Scale, _db);
        }

        /// <summary>
        /// Тонкая
        /// </summary>
        protected void AppendThin(Entity draw)
        {
            Append(draw, _decor.Decor.DecorContinuousThin);
        }

        /// <summary>
        /// Толстая
        /// </summary>
        protected void AppendThick(Entity draw)
        {
            Append(draw, _decor.Decor.DecorContinuousThick);
        }

        /// <summary>
        /// Штриховая
        /// </summary>
        protected void AppendDot(Entity draw)
        {
            Append(draw, _decor.Decor.DecorDot);
        }

        protected void AppendHatch(Hatch h, HatchDecor decor, double? angle = null)
        {
            Append(h, decor);
            if (angle != null) h.PatternAngle = angle.Value;
        }

        protected void AppendHatchConcrete(Hatch h, double? angle = null)
        {
            AppendHatch(h, _decor.Decor.DecorConcrete, angle);
        }

        protected void AppendHatchEarth(Hatch h, double? angle = null)
        {
            AppendHatch(h, _decor.Decor.DecorEarth, angle);
        }
    }
}