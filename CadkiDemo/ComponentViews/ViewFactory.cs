﻿using System;
using System.Collections.Generic;
using CadkiDemo.Components.BusinessEntities.FoundationPitEntities;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Components.Repositories;
using CadkiDemo.ComponentViews.Blocks;
using CadkiDemo.ComponentViews.Plan;
using CadkiDemo.ComponentViews.Section;
using CadkiDemo.Wells;

namespace CadkiDemo.ComponentViews
{
    public class ViewFactory
    {
        private readonly Dictionary<(Type, WellViewType), Type> _views;
        private readonly BlockRepository _blockRepository;

        public ViewFactory()
        {
            _views = new Dictionary<(Type, WellViewType), Type>
            {
                {(typeof(BasePlate),WellViewType.Plan), typeof(BasePlate2dPlanView)},
                {(typeof(BottomPlate),WellViewType.Plan), typeof(BottomPlate2dPlanView)},
                {(typeof(FloorSlab),WellViewType.Plan), typeof(FloorSlab2dPlanView)},
                {(typeof(Ring),WellViewType.Plan), typeof(Ring2dPlanView)},
                {(typeof(RoadPlate),WellViewType.Plan), typeof(RoadPlate2dPlanView)},
                {(typeof(SupportRing),WellViewType.Plan), typeof(SupportRing2dPlanView)},
                {(typeof(WorkingChamber),WellViewType.Plan), typeof(WorkingChamber2dPlanView)},
                {(typeof(Luke),WellViewType.Plan), typeof(Luke2dPlanView)},
                {(typeof(Raintaker),WellViewType.Plan), typeof(Raintaker2dPlanView)},

                {(typeof(BasePlate), WellViewType.Section), typeof(BasePlate2dSectionView)},
                {(typeof(BottomPlate), WellViewType.Section), typeof(BottomPlate2dSectionView)},
                {(typeof(FloorSlab), WellViewType.Section), typeof(FloorSlab2dSectionView)},
                {(typeof(Ring), WellViewType.Section), typeof(Ring2dSectionView)},
                {(typeof(RoadPlate), WellViewType.Section), typeof(RoadPlate2dSectionView)},
                {(typeof(SupportRing), WellViewType.Section), typeof(SupportRing2dSectionView)},
                {(typeof(WorkingChamber), WellViewType.Section), typeof(WorkingChamber2dSectionView)},
                {(typeof(Luke), WellViewType.Section), typeof(Luke2dSectionView)},
                {(typeof(Raintaker), WellViewType.Section), typeof(Raintaker2dSectionView)},
            };

            _blockRepository = new BlockRepository();
        }

        public IComponentView? GetView(AbstractWellComponent component, WellViewType viewType)
        {
            // Поиск блока компонента в ресурсах
            var view = GetBlockView(component, viewType);
            if (view != null) return view;

            _views.TryGetValue((component.GetType(), viewType), out var type);
            if (type == null) return null;
            return (IComponentView) Activator.CreateInstance(type);
        }

        private IComponentView? GetBlockView(AbstractWellComponent component, WellViewType viewType)
        {
            var blockFile = _blockRepository.LoadBlockFile(component.GOST, component.Name);
            if (blockFile == null) return null;

            return viewType switch
            {
                WellViewType.Plan => (IComponentView?) new BlockComponentPlanView(blockFile),
                WellViewType.Section => new BlockComponentSectionView(blockFile),
                _ => null
            };
        }
    }
}