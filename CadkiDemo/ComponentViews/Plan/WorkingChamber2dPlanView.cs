using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Extension;

namespace CadkiDemo.ComponentViews.Plan
{
    public class WorkingChamber2dPlanView : ComponentViewAbstract<WorkingChamber>
    {
        protected override void Draw(WorkingChamber item)
        {
            var center = new Point3d();
            var radius = item.Diametr * 0.5;
            var outerCircle = new Circle(center, Vector3d.ZAxis, radius);
            AppendThick(outerCircle);

            var innerRadius = radius - item.Thickness;
            var innerCircle = new Circle(center, Vector3d.ZAxis, innerRadius);
            AppendThick(innerCircle);

            var h = outerCircle.Id.CreateHatch(innerCircle.Id);
            AppendHatchConcrete(h);
        }
    }
}