using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Extension;

namespace CadkiDemo.ComponentViews.Plan
{
    public class BasePlate2dPlanView : ComponentViewAbstract<BasePlate>
    {
        protected override void Draw(BasePlate plate)
        {
            // Внешний прямоугольник
            var center = new Point2d();
            var squarePl = center.Rectangle(plate.Length, plate.Length);
            AppendThick(squarePl);

            // Внутренний круг
            var radius = plate.Diametr * 0.5;
            var circle = new Circle(center.Point3d(), Vector3d.ZAxis, radius);
            AppendThick(circle);

            // Штриховка
            var h = squarePl.Id.CreateHatch(circle.Id);
            AppendHatchConcrete(h);
        }
    }
}