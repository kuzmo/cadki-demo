﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadkiDemo.ComponentViews.Plan
{
    public class Luke2dPlanView : ComponentViewAbstract<Luke>
    {
        protected override void Draw(Luke luke)
        {
            var center = new Point3d();
            var outerRadius = luke.Diametr * 0.5;
            var outerCircle = new Circle(center, Vector3d.ZAxis, outerRadius);
            AppendThick(outerCircle);
        }
    }
}
