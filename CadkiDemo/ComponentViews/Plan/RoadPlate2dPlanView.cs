using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Extension;

namespace CadkiDemo.ComponentViews.Plan
{
    public class RoadPlate2dPlanView : ComponentViewAbstract<RoadPlate>
    {
        protected override void Draw(RoadPlate plate)
        {
            var center = new Point3d();

            // Внешний прямоугольник
            var recPl = center.Rectangle(plate.Length, plate.Width);
            AppendThick(recPl);

            // Внутренний круг
            var radius = plate.HoleDiametr * 0.5;
            var circle = new Circle(center, Vector3d.ZAxis, radius);
            AppendThick(circle);

            // Уступок
            var legR = radius + 100;
            var legPl = new Polyline(4);
            var legPt = new Point2d(0, -legR);
            legPl.AddVertexAt(0, legPt, 0, 0, 0);
            legPt = legPt.Move(legR, 0);
            legPl.AddVertexAt(0, legPt, 0, 0, 0);
            legPt = legPt.Move(0, legR * 2);
            legPl.AddVertexAt(0, legPt, 0, 0, 0);
            legPt = legPt.Move(-legR, 0);
            legPl.AddVertexAt(0, legPt, 0, 0, 0);
            legPt = legPt.Move(0, -legR * 2);
            legPl.AddVertexAt(0, legPt, -1, 0, 0);
            legPl.Closed = true;
            AppendThin(legPl);

            var h = recPl.Id.CreateHatch(legPl.Id);
            AppendHatchConcrete(h);
        }
    }
}