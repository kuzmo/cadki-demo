using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Extension;

namespace CadkiDemo.ComponentViews.Plan
{
    public class SupportRing2dPlanView : ComponentViewAbstract<SupportRing>
    {
        protected override void Draw(SupportRing ring)
        {
            // Внешний круг
            var center = new Point3d();
            var outerRadius = ring.Diametr * 0.5;
            var outerCircle = new Circle(center, Vector3d.ZAxis, outerRadius);
            AppendThick(outerCircle);

            // Внутренний круг
            var innerRadius = outerRadius - ring.Thickness;
            var innerCircle = new Circle(center, Vector3d.ZAxis, innerRadius);
            AppendThick(innerCircle);

            var h = outerCircle.Id.CreateHatch(innerCircle.Id);
            AppendHatchConcrete(h);
        }
    }
}