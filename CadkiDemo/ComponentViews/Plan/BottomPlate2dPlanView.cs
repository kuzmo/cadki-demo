using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Extension;

namespace CadkiDemo.ComponentViews.Plan
{
    public class BottomPlate2dPlanView : ComponentViewAbstract<BottomPlate>
    {
        protected override void Draw(BottomPlate plate)
        {
            var center = new Point3d();
            var radius = plate.Diametr * 0.5;

            var circle = new Circle(center, Vector3d.ZAxis, radius);
            AppendThick(circle);
        }
    }
}