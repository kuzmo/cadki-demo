﻿using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Extension;
using System.Collections.Generic;

namespace CadkiDemo.ComponentViews.Plan
{
    public class Raintaker2dPlanView : ComponentViewAbstract<Raintaker>
    {
        protected override void Draw(Raintaker component)
        {
            var polylinePoints = CreateGeometry(component);
            var clPl = polylinePoints.CreatePolyline(true);
            AppendThick(clPl);
        }

        private List<Point2d> CreateGeometry(Raintaker component)
        {
            var halfLength = component.Length / 2;
            var halfWidth = component.Width / 2;
            return new List<Point2d> {
                new Point2d(-halfLength, -halfWidth),
                new Point2d(-halfLength, halfWidth),
                new Point2d(halfLength, halfWidth),
                new Point2d(halfLength, -halfWidth)
            };
        }
    }
}
