﻿using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using CadkiDemo.Components.BusinessEntities.WellEntities;
using CadkiDemo.Extension;

namespace CadkiDemo.ComponentViews.Plan
{
    public class FloorSlab2dPlanView : ComponentViewAbstract<FloorSlab>
    {
        protected override void Draw(FloorSlab floorSlab)
        {
            var center = new Point3d();
            var radius = floorSlab.Diametr / 2;
            var len = floorSlab.Diametr;

            // Внешний квадратный контур
            var pt1 = new Point2d(center.X - radius, center.Y - radius);
            var pt2 = pt1.Move(0, len);
            var pt3 = pt2.Move(len, 0);
            var pt4 = pt3.Move(0, -len);
            var plSquare = new List<Point2d> { pt1, pt2, pt3, pt4 };
            var outerSquare = plSquare.CreatePolyline(true);
            AppendThick(outerSquare);

            // Внешний круг
            var outerCircle = new Circle(center, Vector3d.ZAxis, radius)
            {
                LineWeight = LineWeight.LineWeight015,
                LinetypeId = _db.GetDotLineType()
            };

            AppendThin(outerCircle);

            // Внутренний круг
            var holeCenter = center.Move(floorSlab.HoleOffset, 0);
            var holeRadius = floorSlab.HoleDiametr * 0.5;
            var holeCircle = new Circle(holeCenter, Vector3d.ZAxis, holeRadius)
            {
                LineWeight = LineWeight.LineWeight015,
                LinetypeId = _db.GetDotLineType()
            };

            AppendDot(holeCircle);

            var h = outerSquare.Id.CreateHatch(holeCircle.Id);
            AppendHatchConcrete(h);
        }
    }
}
